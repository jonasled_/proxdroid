package de.jonasled.proxdroid;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.preference.PreferenceManager;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.net.ssl.HttpsURLConnection;

public class NodeControlActivity extends AppCompatActivity {

    boolean showLoading = true;
    Button consoleButton;
    Button shutdownButton;
    Button restartButton;
    Button notesButton;
    TextView nameText;
    TextView idText;
    TextView ramText;
    TextView cpuText;
    TextView driveText;
    TextView statusText;
    TextView ksmSharingText;
    TextView swapText;
    JSONObject vmConfig;
    JSONObject vmConfig2;
    ImageView typeLogo;
    LineChart ramGraph;
    LineChart cpuGraph;
    LineChart ioGraph;
    LineChart utilizationGraph;
    LineChart networkGraph;
    View squareNetworkIn;
    View squareNetworkOut;
    Spinner graphSpinner;
    Spinner avgMaxSpinner;
    Timer timer;
    List<Entry> ramGraphSeries;
    List<Entry> cpuGraphSeries;
    List<Entry> ioGraphSeries;
    List<Entry> utilizationGraphSeries;
    List<Entry> networkInGraphSeries;
    List<Entry> networkOutGraphSeries;
    ArrayAdapter<CharSequence> graphSpinnerValues;
    ArrayAdapter<CharSequence> avgMaxSpinnerValues;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(startActivity.theme == 3) setTheme(R.style.Theme_App_black);
        setContentView(R.layout.activity_node_control);
        Toolbar toolbar = findViewById(R.id.toolbar);
        consoleButton = findViewById(R.id.buttonConsole);
        shutdownButton = findViewById(R.id.buttonShutdown);
        restartButton = findViewById(R.id.buttonRestart);
        notesButton = findViewById(R.id.buttonNotes);
        nameText = findViewById(R.id.labelName);
        idText = findViewById(R.id.labelID);
        ksmSharingText = findViewById(R.id.labelKSM);
        swapText = findViewById(R.id.labelSWAP);
        typeLogo = findViewById(R.id.typeLogo);
        ramText = findViewById(R.id.labelRAM);
        cpuText = findViewById(R.id.labelCPU);
        driveText = findViewById(R.id.labelDrive);
        ramGraph = findViewById(R.id.graphRAM);
        cpuGraph = findViewById(R.id.graphCPU);
        ioGraph = findViewById(R.id.graphIO);
        utilizationGraph = findViewById(R.id.graphUtilization);
        statusText = findViewById(R.id.labelStatus);
        networkGraph = findViewById(R.id.graphNetwork);
        squareNetworkIn = findViewById(R.id.squareNetIn);
        squareNetworkOut = findViewById(R.id.squareNetOut);
        graphSpinner = findViewById(R.id.spinnerTime);
        avgMaxSpinner = findViewById(R.id.spinnerAvgMax);

        setSupportActionBar(toolbar);

        setTitle(sessionInfo.machineName);
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                new getConfig().execute();
            }
        }, 0, 10000);


        consoleButton.setOnClickListener((View v) -> {
            Intent intent = new Intent(getApplicationContext(), de.jonasled.proxdroid.ConsoleView.class);
            if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean("useXterm", false)){
                intent.putExtra("url", sessionInfo.baseURL + "?console=shell&vmid=0&node=" + sessionInfo.nodeName + "&resize=scale&xtermjs=1");
            }else {
                intent.putExtra("url", sessionInfo.baseURL + "?console=shell&vmid=0&node=" + sessionInfo.nodeName + "&resize=scale&novnc=1");
            }
            startActivity(intent);
        });

        graphSpinnerValues = ArrayAdapter.createFromResource(this, R.array.spinnerGraph, android.R.layout.simple_spinner_dropdown_item);
        graphSpinner.setAdapter(graphSpinnerValues);
        graphSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(!showLoading){
                    new getConfig().execute();
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
                return;
            }
        });

        avgMaxSpinnerValues = ArrayAdapter.createFromResource(this, R.array.avgMaxSpinner, android.R.layout.simple_spinner_dropdown_item);
        avgMaxSpinner.setAdapter(avgMaxSpinnerValues);
        avgMaxSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(!showLoading){
                    new getConfig().execute();
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
                return;
            }
        });

        notesButton.setOnClickListener((View v) -> new getNotes().execute());
        shutdownButton.setOnClickListener((View v) -> new startStopAction().execute("shutdown"));
        restartButton.setOnClickListener((View v) -> new startStopAction().execute("reboot"));

    }

    private class getNotes extends AsyncTask<String, String, String> {
        private ProgressDialog pd;
        private String description = "";

        @Override
        protected void onPreExecute() {
            HelperFunctions.buildKeystore();
            super.onPreExecute();
            pd = ProgressDialog.show(NodeControlActivity.this, "", getResources().getString(R.string.loading), true,
                    false); // Create and show Progress dialog

        }
        @Override
        protected String doInBackground(String... strings) {
            try {
                URL url = new URL(sessionInfo.baseURL + "api2/json/nodes/" + sessionInfo.nodeName + "/config");
                HttpsURLConnection conn2 = (HttpsURLConnection) url.openConnection();
                conn2.setSSLSocketFactory(HelperFunctions.context.getSocketFactory());
                conn2.setHostnameVerifier(HelperFunctions.hostnameVerifier);
                switch (sessionInfo.loginMethod){
                    case 0:
                        conn2.setRequestProperty("Cookie", "PVEAuthCookie=" + sessionInfo.ticket);//set the auth cookie
                        break;
                    case 1:
                        conn2.addRequestProperty("Authorization", sessionInfo.authString);
                        break;
                }
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn2.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    // Append server response in string
                    sb.append(line + "\n");
                }
                JSONObject resourcesJSON = (JSONObject) (new JSONObject(sb.toString())).get("data");
                try{
                    description = resourcesJSON.getString("description");
                } catch (Exception e) {}


            } catch (IOException e){
                e.printStackTrace();
                return "failedIO";
            } catch (JSONException e){
                e.printStackTrace();
                return "failedJSON";
            }

            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            pd.dismiss();
            if (result == "failedIO") {
                timer.cancel();
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(NodeControlActivity.this);
                dlgAlert.setMessage(R.string.errNotReachable);
                dlgAlert.setTitle(R.string.errNotReachableTitle);
                dlgAlert.setCancelable(false);
                dlgAlert.setPositiveButton(R.string.ok,(DialogInterface dialog, int which) -> finish());
                dlgAlert.create().show();
            } else if (result == "failedJSON") {
                timer.cancel();
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(NodeControlActivity.this);
                dlgAlert.setMessage(R.string.errInvalidJson);
                dlgAlert.setTitle(R.string.errInvalidJsonTitle);
                dlgAlert.setCancelable(false);
                dlgAlert.setPositiveButton(R.string.ok,(DialogInterface dialog, int which) -> finish());
                dlgAlert.create().show();
            }
            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(NodeControlActivity.this);
            dlgAlert.setTitle(R.string.notes);
            dlgAlert.setMessage(description);
            dlgAlert.setPositiveButton(R.string.ok, null);
            dlgAlert.create().show();
        }
    }


    private class startStopAction extends AsyncTask<String, String, String> {
        private ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            HelperFunctions.buildKeystore();
            super.onPreExecute();
            pd = ProgressDialog.show(NodeControlActivity.this, "", getResources().getString(R.string.loading), true,
                    false); // Create and show Progress dialog

        }
        @Override
        protected String doInBackground(String... strings) {
            try {
                String action = strings[0];
                URL url = new URL(sessionInfo.baseURL + "api2/json/nodes/" + sessionInfo.nodeName + "/status");
                //open the connection and send POST data
                HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                conn.setSSLSocketFactory(HelperFunctions.context.getSocketFactory());
                conn.setHostnameVerifier(HelperFunctions.hostnameVerifier);
                conn.setRequestProperty("CSRFPreventionToken", sessionInfo.CSRFPreventionToken);
                switch (sessionInfo.loginMethod){
                    case 0:
                        conn.setRequestProperty("Cookie", "PVEAuthCookie=" + sessionInfo.ticket);//set the auth cookie
                        conn.setRequestProperty("CSRFPreventionToken", sessionInfo.CSRFPreventionToken);
                        break;
                    case 1:
                        conn.addRequestProperty("Authorization", sessionInfo.authString);
                        break;
                }
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write("command=" + action);
                wr.flush();

                //get the response from the server
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;

                while ((line = reader.readLine()) != null) {
                    // Append server response in string
                    sb.append(line + "\n");
                }

            } catch (IOException e){
                e.printStackTrace();
                return "failedIO";
            }

            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            pd.dismiss();
            if (result == "failedIO") {
                timer.cancel();
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(NodeControlActivity.this);
                dlgAlert.setMessage(R.string.errNotReachable);
                dlgAlert.setTitle(R.string.errNotReachableTitle);
                dlgAlert.setCancelable(false);
                dlgAlert.setPositiveButton(R.string.ok,(DialogInterface dialog, int which) -> finish());
                dlgAlert.create().show();
            } else if (result == "failedJSON") {
                timer.cancel();
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(NodeControlActivity.this);
                dlgAlert.setMessage(R.string.errInvalidJson);
                dlgAlert.setTitle(R.string.errInvalidJsonTitle);
                dlgAlert.setCancelable(false);
                dlgAlert.setPositiveButton(R.string.ok,(DialogInterface dialog, int which) -> finish());
                dlgAlert.create().show();
            }


            Timer timer2 = new Timer();
            timer2.schedule(new TimerTask() {
                public void run() {
                    new getConfig().execute();
                }
            }, 2000);
        }
    }

    private class getConfig extends AsyncTask<String, String, String> {
        private ProgressDialog pd;
        int spinnerID;
        int spinner2ID;

        @Override
        protected void onPreExecute() {
            HelperFunctions.buildKeystore();
            NodeControlActivity.this.runOnUiThread(() -> {
                super.onPreExecute();
                if(showLoading) {
                    pd = ProgressDialog.show(NodeControlActivity.this, "", getResources().getString(R.string.getDataOf) + " " + sessionInfo.nodeName, true,
                            false); // Create and show Progress dialog
                }
                ramGraphSeries = new ArrayList<Entry>();
                cpuGraphSeries = new ArrayList<Entry>();
                ioGraphSeries = new ArrayList<Entry>();
                utilizationGraphSeries = new ArrayList<Entry>();
                networkInGraphSeries = new ArrayList<Entry>();
                networkOutGraphSeries = new ArrayList<Entry>();

            });
            spinnerID = graphSpinner.getSelectedItemPosition();
            spinner2ID = avgMaxSpinner.getSelectedItemPosition();

        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                URL url = new URL( sessionInfo.baseURL + "api2/json/nodes/" + sessionInfo.nodeName + "/status");
                HttpsURLConnection conn2 = (HttpsURLConnection) url.openConnection();
                conn2.setSSLSocketFactory(HelperFunctions.context.getSocketFactory());
                conn2.setHostnameVerifier(HelperFunctions.hostnameVerifier);
                switch (sessionInfo.loginMethod){
                    case 0:
                        conn2.setRequestProperty("Cookie", "PVEAuthCookie=" + sessionInfo.ticket);//set the auth cookie
                        break;
                    case 1:
                        conn2.addRequestProperty("Authorization", sessionInfo.authString);
                        break;
                }
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn2.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    // Append server response in string
                    sb.append(line + "\n");
                }
                vmConfig = (JSONObject) (new JSONObject(sb.toString())).get("data");

                url = new URL( sessionInfo.baseURL + "api2/json/cluster/resources");
                conn2 = (HttpsURLConnection) url.openConnection();
                conn2.setSSLSocketFactory(HelperFunctions.context.getSocketFactory());
                conn2.setHostnameVerifier(HelperFunctions.hostnameVerifier);
                switch (sessionInfo.loginMethod){
                    case 0:
                        conn2.setRequestProperty("Cookie", "PVEAuthCookie=" + sessionInfo.ticket);//set the auth cookie
                        break;
                    case 1:
                        conn2.addRequestProperty("Authorization", sessionInfo.authString);
                        break;
                }
                reader = new BufferedReader(new InputStreamReader(conn2.getInputStream()));
                sb = new StringBuilder();
                while ((line = reader.readLine()) != null) {
                    // Append server response in string
                    sb.append(line + "\n");
                }
                JSONArray resourcesJSON = (JSONArray) (new JSONObject(sb.toString())).get("data");
                for(int i=0; i < resourcesJSON.length(); i++){
                    JSONObject temp = resourcesJSON.getJSONObject(i);
                    try{
                        if (temp.getString("id").equals(sessionInfo.machineID)){
                            vmConfig2 = temp;
                            break;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


                String timeframe = "hour";
                switch (spinnerID){
                    case 0:
                        timeframe = "hour";
                        break;
                    case 1:
                        timeframe = "day";
                        break;
                    case 2:
                        timeframe = "week";
                        break;
                    case 3:
                        timeframe = "month";
                        break;
                    case 4:
                        timeframe = "year";
                        break;
                }
                String cf = "AVERAGE";
                switch (spinnerID){
                    case 0:
                        cf = "AVERAGE";
                        break;
                    case 1:
                        cf = "MAX";
                        break;
                }

                url = new URL(sessionInfo.baseURL + "api2/json/nodes/" + sessionInfo.nodeName + "/rrddata?timeframe=" + timeframe + "&cf=" + cf);
                conn2 = (HttpsURLConnection) url.openConnection();
                conn2.setSSLSocketFactory(HelperFunctions.context.getSocketFactory());
                conn2.setHostnameVerifier(HelperFunctions.hostnameVerifier);
                switch (sessionInfo.loginMethod){
                    case 0:
                        conn2.setRequestProperty("Cookie", "PVEAuthCookie=" + sessionInfo.ticket);//set the auth cookie
                        break;
                    case 1:
                        conn2.addRequestProperty("Authorization", sessionInfo.authString);
                        break;
                }
                reader = new BufferedReader(new InputStreamReader(conn2.getInputStream()));
                sb = new StringBuilder();
                while ((line = reader.readLine()) != null) {
                    // Append server response in string
                    sb.append(line + "\n");
                }
                JSONArray graphJson = (JSONArray) (new JSONObject(sb.toString())).get("data");

                for(int i=0; i < graphJson.length(); i++) {
                    JSONObject temp = graphJson.getJSONObject(i);
                    try{
                        ramGraphSeries.add(new Entry(temp.getLong("time") * 1000, (float) temp.getDouble("memused") ));
                        cpuGraphSeries.add(new Entry(temp.getLong("time") * 1000, (float) temp.getDouble("cpu") * 100));
                        ioGraphSeries.add(new Entry(temp.getLong("time") * 1000, (float) (temp.getDouble("iowait")) * 100));
                        utilizationGraphSeries.add(new Entry(temp.getLong("time") * 1000, (float) temp.getDouble("loadavg")));
                        networkInGraphSeries.add(new Entry(temp.getLong("time") * 1000, (float) temp.getDouble("netin")));
                        networkOutGraphSeries.add(new Entry(temp.getLong("time") * 1000, (float) temp.getDouble("netout")));
                    } catch (Exception e) {e.printStackTrace();} //Sometimes one entry is missing

                }




            } catch (IOException e){
                e.printStackTrace();
                return "failedIO";
            } catch (JSONException e){
                e.printStackTrace();
                return "failedJSON";
            }

            return "";

        }

        @Override
        protected void onPostExecute(String result) {
            NodeControlActivity.this.runOnUiThread(() -> {
                boolean graphAnimation = false;
                if (showLoading) {
                    pd.dismiss();
                    graphAnimation = true;
                    showLoading = false;
                }
                if (result == "failedIO") {
                    timer.cancel();
                    AlertDialog.Builder dlgAlert = new AlertDialog.Builder(NodeControlActivity.this);
                    dlgAlert.setMessage(R.string.errNotReachable);
                    dlgAlert.setTitle(R.string.errNotReachableTitle);
                    dlgAlert.setCancelable(false);
                    dlgAlert.setPositiveButton(R.string.ok,(DialogInterface dialog, int which) -> finish());
                    dlgAlert.create().show();
                    return;
                } else if (result == "failedJSON") {
                    timer.cancel();
                    AlertDialog.Builder dlgAlert = new AlertDialog.Builder(NodeControlActivity.this);
                    dlgAlert.setMessage(R.string.errInvalidJson);
                    dlgAlert.setTitle(R.string.errInvalidJsonTitle);
                    dlgAlert.setCancelable(false);
                    dlgAlert.setPositiveButton(R.string.ok,(DialogInterface dialog, int which) -> finish());
                    dlgAlert.create().show();
                    return;
                }
                try{
                    sessionInfo.machineName = sessionInfo.nodeName;
                    float cpuUsage = Math.round(vmConfig.getDouble("cpu") * 10000);
                    cpuUsage /= 100;

                    nameText.setText(sessionInfo.machineName);
                    idText.setText(sessionInfo.machineID);
                    ramText.setText(HelperFunctions.humanReadableByteCountBin(vmConfig.getJSONObject("memory").getLong("used")) + " / " + HelperFunctions.humanReadableByteCountBin(vmConfig.getJSONObject("memory").getLong("total")));
                    cpuText.setText( cpuUsage + "%");
                    driveText.setText(HelperFunctions.humanReadableByteCountBin(vmConfig.getJSONObject("rootfs").getLong("used")) + " / " + HelperFunctions.humanReadableByteCountBin(vmConfig.getJSONObject("rootfs").getLong("total")));
                    ksmSharingText.setText(HelperFunctions.humanReadableByteCountBin(vmConfig.getJSONObject("ksm").getLong("shared")));
                    swapText.setText(HelperFunctions.humanReadableByteCountBin(vmConfig.getJSONObject("swap").getLong("used")) + " / " + HelperFunctions.humanReadableByteCountBin(vmConfig.getJSONObject("swap").getLong("total")));
                    if(vmConfig2.getString("status").equals("online")){
                        statusText.setText(R.string.online);
                        shutdownButton.setEnabled(true);
                        restartButton.setEnabled(true);
                        consoleButton.setEnabled(true);
                    } else {
                        statusText.setText(R.string.offline);
                        shutdownButton.setEnabled(false);
                        restartButton.setEnabled(false);
                        consoleButton.setEnabled(false);
                    }



                    graphHandler.setupGraph(ramGraph, graphSpinner.getSelectedItemPosition() <= 1, true, graphAnimation, NodeControlActivity.this, ramGraphSeries);
                    graphHandler.setupGraph(cpuGraph, graphSpinner.getSelectedItemPosition() <= 1, false, graphAnimation, NodeControlActivity.this, cpuGraphSeries);
                    graphHandler.setupGraph(ioGraph, graphSpinner.getSelectedItemPosition() <= 1, true, graphAnimation, NodeControlActivity.this, ioGraphSeries);
                    graphHandler.setupGraph(utilizationGraph, graphSpinner.getSelectedItemPosition() <= 1, true, graphAnimation, NodeControlActivity.this, utilizationGraphSeries);
                    graphHandler.setupGraph(networkGraph, graphSpinner.getSelectedItemPosition() <= 1, true, graphAnimation, NodeControlActivity.this, networkInGraphSeries, networkOutGraphSeries);

                } catch (JSONException e){
                    timer.cancel();
                    e.printStackTrace();
                    AlertDialog.Builder dlgAlert = new AlertDialog.Builder(NodeControlActivity.this);
                    dlgAlert.setMessage(R.string.errInvalidJson);
                    dlgAlert.setTitle(R.string.errInvalidJsonTitle);
                    dlgAlert.setCancelable(false);
                    dlgAlert.setPositiveButton(R.string.ok,(DialogInterface dialog, int which) -> finish());
                    dlgAlert.create().show();
                }
            });

        }

    }
}

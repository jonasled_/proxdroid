package de.jonasled.proxdroid;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import android.content.SharedPreferences;
import androidx.preference.PreferenceManager;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.LineGraphSeries;

import org.apache.commons.codec.binary.Base32;
import org.apache.commons.codec.binary.Hex;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidParameterSpecException;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.*;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import de.taimos.totp.TOTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class HelperFunctions {
    public static KeyStore keyStore;
    public static String tmfAlgorithm;
    public static TrustManagerFactory tmf;
    public static SSLContext context;
    public static HostnameVerifier hostnameVerifier;
    public static String lastConnectName = ""; //we declare this variable here, becuase if we restart the ClusterActivity Class it will reset the variable


    public static String humanReadableByteCountBin(long bytes) { //Convert a byte long to a String with suitable extension
        long absB = bytes == Long.MIN_VALUE ? Long.MAX_VALUE : Math.abs(bytes);//get the absolute value (that means remove all leading symbols ( -5 ==> 5))
        if (absB < 1024) { //if the value is smaller than 1024 we use Byte
            return bytes + " B";
        }
        long value = absB;
        CharacterIterator ci = new StringCharacterIterator("KMGTPE");//while the value is bigger than 1024, sbtract 1024 and switch to next unit size
        for (int i = 40; i >= 0 && absB > 0xfffccccccccccccL >> i; i -= 10) {
            value >>= 10;
            ci.next();
        }
        value *= Long.signum(bytes);
        return String.format("%.1f %ciB", value / 1024.0, ci.current()); //return the size as pretty formatted String
    }

    public static void formatGraph(GraphView graphView, LineGraphSeries... lineGraphSeries){
        double maxY = lineGraphSeries[0].getHighestValueY();
        double minY = lineGraphSeries[0].getLowestValueY();
        double maxX = lineGraphSeries[0].getHighestValueX();
        double minX = lineGraphSeries[0].getLowestValueX();

        graphView.removeAllSeries();
        for (LineGraphSeries series: lineGraphSeries) {
            graphView.addSeries(series);

            if(series.getHighestValueY() > maxY) maxY = series.getHighestValueY();
            if(series.getLowestValueY() < minY) minY = series.getLowestValueY();
            if(series.getHighestValueX() > maxX) maxX = series.getHighestValueX();
            if(series.getLowestValueX() < minX) minX = series.getLowestValueX();
        }

        graphView.getViewport().setYAxisBoundsManual(true);
        graphView.getViewport().setMaxY(maxY + 1);
        graphView.getViewport().setMinY(minY - 1);
        graphView.getViewport().setXAxisBoundsManual(true);
        graphView.getViewport().setMaxX(maxX + 1);
        graphView.getViewport().setMinX(minX - 1);
    }

    public  static String totpGenerator(String secretKey) { //used to generate the TFA tokens on open connection, if TFA is enabled
        Base32 base32 = new Base32();
        byte[] bytes = base32.decode(secretKey); //convert the Key to Hex format
        String hexKey = Hex.encodeHexString(bytes);
        return TOTP.getOTP(hexKey);//caclculate the TOTP key and return it as String
    }

    public static int getFirstTimeRun(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        int result, currentVersionCode = BuildConfig.VERSION_CODE;
        int lastVersionCode = sp.getInt("FIRSTTIMERUN", -1);
        if (lastVersionCode == -1) result = 0; else
            result = (lastVersionCode == currentVersionCode) ? 2 : 1;
        sp.edit().putInt("FIRSTTIMERUN", currentVersionCode).apply();
        if(!sp.getBoolean("firstrun", true)) result = 1;
        return result;
    }

    public static void buildKeystore (){ //build the own keystore to verify selfSigned certificates
        try {
            hostnameVerifier = new HostnameVerifier() {
                @Override
                public boolean verify(String hostname_, SSLSession session) {
                    return hostname_.equals(sessionInfo.hostname);
                }
            };

            keyStore = KeyStore.getInstance("AndroidKeyStore"); //load the keystore (AndroidKeyStore is persistent)
            keyStore.load(null, null);
            // Create a TrustManager that trusts the CAs in our KeyStore
            tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            tmf.init(keyStore);

            // Create an SSLContext that uses our TrustManager. The GET / POST requests uses this to verify the cert.
            context = SSLContext.getInstance("TLS");
            context.init(null, tmf.getTrustManagers(), null);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void trustCert(Context context, String url){//This function is used to ask for a new cert to trust.

        try {
            String[] result = new String[6];
            URL urlURL = new URL(url + "/");

            SSLContext sslCtx = SSLContext.getInstance("TLS"); //create a new SSL context, which trusts all Certificates
            sslCtx.init(null, new TrustManager[]{ new X509TrustManager() {

                private X509Certificate[] accepted;

                @Override
                public void checkClientTrusted(X509Certificate[] xcs, String string) throws CertificateException {
                }

                @Override
                public void checkServerTrusted(X509Certificate[] xcs, String string) throws CertificateException {
                    accepted = xcs;
                }

                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return accepted;
                }
            }}, null);

            HttpsURLConnection connection = (HttpsURLConnection) urlURL.openConnection(); //open the URL submitted with the Parameter

            connection.setHostnameVerifier((String string, SSLSession ssls) -> true);

            connection.setSSLSocketFactory(sslCtx.getSocketFactory());
            connection.connect();
            Certificate[] certificates = connection.getServerCertificates();


            for (int i = 0; i < certificates.length; i++) { //for every certificate in chain ask if we should trust it.
                final boolean[] returnBool = new boolean[1];
                Certificate certificate;
                certificate = certificates[i];
                X509Certificate c = (X509Certificate) certificate;

                result[0] = c.getSubjectDN().toString();
                result[1] = c.getIssuerDN().toString();
                result[2] = c.getNotBefore().toString();
                result[2] = c.getNotAfter().toString();
                result[3] = c.getSerialNumber().toString();

                int finalI = i;
                ((Activity) context).runOnUiThread(() -> { //We have to run the alert on the main thread

                    AlertDialog.Builder dlgAlert = new AlertDialog.Builder(context);
                    dlgAlert.setMessage(context.getResources().getString(R.string.issuer) + ": " + result[1] + "\n" + context.getResources().getString(R.string.validFrom) + " " + result[2] + " " + context.getResources().getString(R.string.to) + " " + result[3]);
                    dlgAlert.setTitle(R.string.errUntrustedCertTitle);
                    dlgAlert.setPositiveButton(R.string.accept, (DialogInterface dialog, int which) -> {
                        try {//install the Cert to truststore
                            keyStore.setCertificateEntry(c.getSerialNumber().toString(), certificate);
                        } catch (KeyStoreException e) {
                            e.printStackTrace();
                        }
                        AlertDialog.Builder dlgAlert2 = new AlertDialog.Builder(context); //ask the user to rerun the action
                        dlgAlert2.setMessage(R.string.errRunAgain);
                        dlgAlert2.setTitle(R.string.errRunAgainTitle);
                        dlgAlert2.setPositiveButton(R.string.ok, (DialogInterface dialog2, int which2) -> {});
                        if(finalI == 0) { //only show the dialog on last certificate
                            dlgAlert2.show();
                        }
                    });
                    dlgAlert.setNegativeButton(R.string.deny, (DialogInterface dialog, int which) -> {//close the activity if user denies the cert
                        ((Activity) context).finish();
                    });
                    dlgAlert.create().show();
                });

            }

            connection.disconnect();
        } catch (Exception ex) {//catch all exceptions
            ex.printStackTrace();//print the exception to console
        }
    }


    public static String getRandomString(int sizeOfPasswordString){
        final String ALLOWED_CHARACTERS ="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        final Random random=new Random();
        final StringBuilder sb=new StringBuilder();

        for(int i=0;i<sizeOfPasswordString;++i){
            char currentChar = ALLOWED_CHARACTERS.charAt(random.nextInt(ALLOWED_CHARACTERS.length()));
            sb.append(currentChar);
        }
        return sb.toString();
    }

    public static byte[] encryptMsg(String message)
            throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidParameterSpecException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException
    {
        /* Encrypt the message. */
        Cipher cipher = null;
        cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, startActivity.decryptToken);
        byte[] cipherText = cipher.doFinal(message.getBytes("UTF-8"));
        return cipherText;
    }

    public static String decryptMsg(byte[] cipherText)
            throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidParameterSpecException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, UnsupportedEncodingException
    {
        /* Decrypt the message, given derived encContentValues and initialization vector. */
        Cipher cipher = null;
        cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, startActivity.decryptToken);
        String decryptString = new String(cipher.doFinal(cipherText), "UTF-8");
        return decryptString;
    }

    public static String encodeValue(String value) {
        try {
            return URLEncoder.encode(value, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex.getCause());
        }
    }

    public static JSONArray sortJson(JSONArray unsortedArray, String keyName) throws JSONException {
        JSONArray sortedJsonArray = new JSONArray();
        List<JSONObject> jsonValues = new ArrayList<JSONObject>();
        for (int i = 0; i < unsortedArray.length(); i++) {
            jsonValues.add(unsortedArray.getJSONObject(i));
        }
        Collections.sort( jsonValues, new Comparator<JSONObject>() {

            @Override
            public int compare(JSONObject a, JSONObject b) {
                String valA = new String();
                String valB = new String();

                try {
                    valA = a.get(keyName).toString();
                    valB = b.get(keyName).toString();
                }
                catch (JSONException e) {
                    //do something
                }

                return valA.compareTo(valB);
                //if you want to change the sort order, simply use the following:
                //return -valA.compareTo(valB);
            }
        });

        for (int i = 0; i < unsortedArray.length(); i++) {
            sortedJsonArray.put(jsonValues.get(i));
        }

        return sortedJsonArray;
    }
}

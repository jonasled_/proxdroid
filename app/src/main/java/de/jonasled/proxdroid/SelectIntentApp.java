package de.jonasled.proxdroid;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.common.primitives.Ints;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class SelectIntentApp extends AppCompatActivity {

    PackageManager pm;
    ListView listView;
    ArrayList<String> appID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_intent_app);
        this.setFinishOnTouchOutside(false);
        listView = findViewById(R.id.listView);

        pm = getApplicationContext().getPackageManager();
        List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);//get a list of installed apps.

        ArrayList<String> appNames = new ArrayList<>();
        ArrayList<Drawable> appIcons = new ArrayList<>();
        appID = new ArrayList<>();

        for (ApplicationInfo packageInfo : packages) {
            if (packageInfo != null) {
                try {
                    appIcons.add(getPackageManager().getApplicationIcon(packageInfo.packageName));
                    appNames.add(pm.getApplicationLabel(packageInfo).toString());
                    appID.add(packageInfo.packageName);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }

        for (int i = 0; i < appNames.size(); i++) { //sort the list alphabetical
            for (int j = i + 1; j < appNames.size(); j++) {
                if (appNames.get(i).toLowerCase().compareTo(appNames.get(j).toLowerCase()) > 0) {
                    String temp = appNames.get(i);
                    appNames.set(i, appNames.get(j));
                    appNames.set(j, temp);

                    temp = appID.get(i);
                    appID.set(i, appID.get(j));
                    appID.set(j, temp);

                    Drawable temp_ = appIcons.get(i);
                    appIcons.set(i, appIcons.get(j));
                    appIcons.set(j, temp_);
                }
            }
        }

        AppListAdapter adapter;
        adapter = new AppListAdapter(this, appNames.toArray(new String[0]) , appIcons.toArray(new Drawable[0]));

        listView.setAdapter(adapter); //set the Adapter on the listview (show the elements)

        listView.setOnItemClickListener((AdapterView<?> parent, View view, int position, long id) -> {//wait for click on listView element
            SharedPreferences mPreferences = PreferenceManager.getDefaultSharedPreferences(SelectIntentApp.this);
            SharedPreferences.Editor prefEdit = mPreferences.edit();
            prefEdit.putString("intentApp", appID.get(position));
            prefEdit.apply();
            finish();
        });
    }
}
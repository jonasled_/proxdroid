package de.jonasled.proxdroid;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.common.primitives.Ints;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;

import javax.net.ssl.HttpsURLConnection;

public class backupActivity extends AppCompatActivity {
    ArrayList<String> backupStorages = new ArrayList<>();
    ArrayList<String> backups = new ArrayList<>();
    de.jonasled.proxdroid.ClusterListAdapter adapter;
    ListView backupListView;
    FloatingActionButton fab;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(startActivity.theme == 3) setTheme(R.style.Theme_App_black);
        setContentView(R.layout.activity_backup);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        backupListView = findViewById(R.id.backupList);
        fab = findViewById(R.id.backupFAB);

        setTitle(sessionInfo.machineName + " - " + getResources().getString(R.string.backup));

        fab.setOnClickListener((View v) -> {
            Intent intent = new Intent(getApplicationContext(), de.jonasled.proxdroid.backupCreateActivity.class);
            startActivity(intent);
        });

        new getConfig().execute();
    }

    private class getConfig extends AsyncTask<String, String, String> {
        private ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            HelperFunctions.buildKeystore();
            pd = ProgressDialog.show(backupActivity.this, "", getResources().getString(R.string.loading), true,//open a loading dialog
                    false);
            backupStorages = new ArrayList<>();
        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                URL url = new URL( sessionInfo.baseURL + "api2/json/nodes/" + sessionInfo.nodeName + "/storage?format=1&content=backup");
                HttpsURLConnection conn2 = (HttpsURLConnection) url.openConnection();
                conn2.setSSLSocketFactory(HelperFunctions.context.getSocketFactory());
                conn2.setHostnameVerifier(HelperFunctions.hostnameVerifier);
                conn2.setRequestProperty("Cookie", "PVEAuthCookie=" + sessionInfo.ticket);
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn2.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    // Append server response in string
                    sb.append(line + "\n");
                }
                JSONArray resourcesJSON = (JSONArray) (new JSONObject(sb.toString())).get("data");
                for(int i=0; i < resourcesJSON.length(); i++){
                    JSONObject temp = resourcesJSON.getJSONObject(i);
                    try {
                        backupStorages.add(temp.getString("storage"));
                    } catch (Exception e) {}
                }

                for (String storage:backupStorages) {
                    url = new URL( sessionInfo.baseURL + "api2/json/nodes/" + sessionInfo.nodeName +"/storage/" +storage + "/content?content=backup");
                    conn2 = (HttpsURLConnection) url.openConnection();
                    conn2.setSSLSocketFactory(HelperFunctions.context.getSocketFactory());
                    conn2.setHostnameVerifier(HelperFunctions.hostnameVerifier);
                    conn2.setRequestProperty("Cookie", "PVEAuthCookie=" + sessionInfo.ticket);
                    reader = new BufferedReader(new InputStreamReader(conn2.getInputStream()));
                    sb = new StringBuilder();
                    while ((line = reader.readLine()) != null) {
                        // Append server response in string
                        sb.append(line + "\n");
                    }
                    resourcesJSON = (JSONArray) (new JSONObject(sb.toString())).get("data");
                    for(int i=0; i < resourcesJSON.length(); i++){
                        JSONObject temp = resourcesJSON.getJSONObject(i);
                        try {
                            if(temp.getString("vmid").equals(sessionInfo.machineID)){
                                backups.add(storage + ": " + temp.getString("volid").split("backup/")[1]);
                            }
                        } catch (Exception e) {e.printStackTrace();}
                    }
                }


            } catch (IOException e){
                e.printStackTrace();
                return "failedIO";
            } catch (JSONException e){
                e.printStackTrace();
                return "failedJSON";
            }

            return "";

        }

        @Override
        protected void onPostExecute(String result) {
            ArrayList sortedKeys = new ArrayList(backups);//The response from proxmox is a mess, no System or whatever, so we sort the array after the key
            Collections.sort(sortedKeys);

            ArrayList<String> label = new ArrayList<>();
            ArrayList<Integer> icon = new ArrayList<>();
            ArrayList<Integer> usage = new ArrayList<>();


            for (Object key : sortedKeys.toArray()) {//parse the sorted key list
                label.add(key.toString());
                icon.add(R.drawable.backup);
                usage.add(-1);
            }
            int[] iconsInt = Ints.toArray(icon);
            int[] usageInt = Ints.toArray(usage);
            String[] labelArray = label.toArray(new String[0]);//convert the List to array and show the listView
            adapter = new de.jonasled.proxdroid.ClusterListAdapter(backupActivity.this, labelArray, iconsInt, usageInt);
            backupListView.setAdapter(adapter);
            pd.dismiss();
        }

    }
}
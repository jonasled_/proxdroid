package de.jonasled.proxdroid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class debugActivity extends AppCompatActivity {

    Button webglTest;
    Button useragent;
    Button xtermDemo;
    Button manualDBUpgrade;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_debug);
        webglTest = findViewById(R.id.buttonWebGL);
        useragent = findViewById(R.id.buttonUseragent);
        xtermDemo = findViewById(R.id.buttonxXermDemo);
        manualDBUpgrade = findViewById(R.id.buttonUpdate);

        webglTest.setOnClickListener((View v) -> {
            Intent intent = new Intent(getApplicationContext(), ConsoleView.class);
            intent.putExtra("ticket", "");
            intent.putExtra("url", "https://get.webgl.org/");
            intent.putExtra("baseurl", "");
            intent.putExtra("CSRFPreventionToken", "");
            startActivity(intent);
        });
        useragent.setOnClickListener((View v) -> {
            Intent intent = new Intent(getApplicationContext(), ConsoleView.class);
            intent.putExtra("ticket", "");
            intent.putExtra("url", "https://www.whatismybrowser.com/detect/what-is-my-user-agent");
            intent.putExtra("baseurl", "");
            intent.putExtra("CSRFPreventionToken", "");
            startActivity(intent);
        });
        xtermDemo.setOnClickListener((View v) -> {
            Intent intent = new Intent(getApplicationContext(), ConsoleView.class);
            intent.putExtra("ticket", "");
            intent.putExtra("url", "https://codercat.tk/terminal-prints.html");
            intent.putExtra("baseurl", "");
            intent.putExtra("CSRFPreventionToken", "");
            startActivity(intent);
        });
        manualDBUpgrade.setOnClickListener((View v) -> {
            de.jonasled.proxdroid.DBHelper mydb = new de.jonasled.proxdroid.DBHelper(this);
            mydb.appUpdate();
        });
    }
}
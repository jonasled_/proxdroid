package de.jonasled.proxdroid;

import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.preference.PreferenceManager;

import java.io.Console;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class startActivity extends AppCompatActivity implements IAuthenticateListener {

    public static SecretKey decryptToken;
    public static int theme = 0;
    private SharedPreferences mPreferences;
    private FingerprintHandler mFingerprintHandler;
    SharedPreferences.Editor editor;
    public int runcode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        runcode = HelperFunctions.getFirstTimeRun(this);

        mPreferences = PreferenceManager.getDefaultSharedPreferences(startActivity.this);

        theme = Integer.parseInt(mPreferences.getString("darkmode", "0"));
        switch (theme){ //if overwritten enable or disable darkmode
            case 1:
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                break;
            case 2:
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                break;
            case 3:
                setTheme(R.style.Theme_App_black);

        }
        setContentView(R.layout.activity_login);

        editor = mPreferences.edit();
        if(runcode == 0){
            SecretKey key = null;
            try {
                key = KeyGenerator.getInstance("AES").generateKey();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            editor.putBoolean("fingerprintEnabled", false);
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    savePassword(Base64.getEncoder().encodeToString(key.getEncoded()));
                } else {
                    savePassword(android.util.Base64.encodeToString(key.getEncoded(), android.util.Base64.DEFAULT));
                }
            } catch (Exception e) {
                e.printStackTrace();
                editor.putBoolean("fingerprintEnabled",false);
            }
            if(!mPreferences.getBoolean("fingerprintEnabled", false)){
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    editor.putString("decryptKey", Base64.getEncoder().encodeToString(key.getEncoded()));
                } else {
                    editor.putString("decryptKey", android.util.Base64.encodeToString(key.getEncoded(), android.util.Base64.DEFAULT));
                }
            }
            editor.apply();
            
            de.jonasled.proxdroid.DBHelper mydb = new de.jonasled.proxdroid.DBHelper(this);
            mydb.appUpdate();
        }
        if(runcode == 1){
            de.jonasled.proxdroid.DBHelper mydb = new de.jonasled.proxdroid.DBHelper(this);
            mydb.appUpdate();
            if(!mPreferences.getBoolean("fingerprintEnabled", false)){
                String decryptPassword = mPreferences.getString("decryptKey", null);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    decryptToken = new SecretKeySpec(Base64.getDecoder().decode(decryptPassword), "AES");
                    savePassword(Base64.getEncoder().encodeToString(decryptToken.getEncoded()));
                } else {
                    decryptToken = new SecretKeySpec(android.util.Base64.decode(decryptPassword, android.util.Base64.DEFAULT), "AES");
                    savePassword(android.util.Base64.encodeToString(decryptToken.getEncoded(), android.util.Base64.DEFAULT));
                }

            }
        }

        if(!mPreferences.getBoolean("fingerprintEnabled", false)){
            onAuthenticate(mPreferences.getString("decryptKey", ""));
        }



    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mPreferences.getBoolean("fingerprintEnabled", false)) initSensor();
        else onAuthenticate(mPreferences.getString("decryptKey", ""));
    }


    private void savePassword(String password) {
        if (Utils.checkSensorState(this)) {
            String encoded = Utils.encryptString(password);
            mPreferences.edit().putString(FingerprintHandler.KEY_PASSWORD, encoded).apply();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mFingerprintHandler != null) {
            mFingerprintHandler.cancel();
        }
    }

    private void initSensor() {
        if (Utils.checkSensorState(this)) {
            FingerprintManager.CryptoObject cryptoObject = Utils.getCryptoObject();
            if (cryptoObject != null) {
                FingerprintManager fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);
                mFingerprintHandler = new FingerprintHandler(this, mPreferences, this);
                mFingerprintHandler.startAuth(fingerprintManager, cryptoObject);
            }
        }
    }
    
    @Override
    public void onAuthenticate(String decryptPassword) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            decryptToken = new SecretKeySpec(Base64.getDecoder().decode(decryptPassword), "AES");
        } else {
            decryptToken = new SecretKeySpec(android.util.Base64.decode(decryptPassword, android.util.Base64.DEFAULT), "AES");
        }
        startActivity(new Intent(this, ServerActivity.class));
        finish();
    }
}

package de.jonasled.proxdroid;

public class sessionInfo {
    public static int databaseID;
    public static int loginMethod;

    public static String baseURL;
    public static String hostname;
    public static String ticket;
    public static String CSRFPreventionToken;
    public static String authString;

    public static String machineID;
    public static String machineType;
    public static String machineName;
    public static String nodeName;
    public static boolean spiceSupported;
}

package de.jonasled.proxdroid;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import android.os.Environment;
import androidx.core.content.FileProvider;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.google.common.collect.Iterators;
import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.preference.PreferenceManager;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.URL;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class ServerControlActivity extends AppCompatActivity {

    boolean showLoading = true;
    Button consoleButton;
    Button startButton;
    Button shutdownButton;
    Button stopButton;
    Button resetButton;
    Button notesButton;
    Button backupButton;
    Button hardwareDetailButton;
    TextView nameText;
    TextView nodeText;
    TextView idText;
    TextView ramText;
    TextView cpuText;
    TextView driveText;
    TextView statusText;
    TextView uptimeText;
    TextView swapText;
    JSONObject vmConfig;
    ImageView typeLogo;
    LineChart ramGraph;
    LineChart cpuGraph;
    LineChart hddGraph;
    LineChart networkGraph;
    View squareDriveRead;
    View squareDriveWrite;
    View squareNetworkIn;
    View squareNetworkOut;
    Spinner graphSpinner;
    Spinner avgMaxSpinner;
    Timer timer;
    List<Entry> ramGraphSeries;
    List<Entry> cpuGraphSeries;
    List<Entry> hddWriteGraphSeries;
    List<Entry> hddReadGraphSeries;
    List<Entry> networkInGraphSeries;
    List<Entry> networkOutGraphSeries;
    ArrayAdapter<CharSequence> graphSpinnerValues;
    ArrayAdapter<CharSequence> avgMaxSpinnerValues;
    LinearLayout graphLayout;
    TableRow swapTableRow;


    TrustManager[] trustAllCerts = new TrustManager[]{
            new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
                public void checkClientTrusted(
                        java.security.cert.X509Certificate[] certs, String authType) {
                }
                public void checkServerTrusted(
                        java.security.cert.X509Certificate[] certs, String authType) {
                }
            }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(startActivity.theme == 3) setTheme(R.style.Theme_App_black);
        setContentView(R.layout.activity_server_control);
        Toolbar toolbar = findViewById(R.id.toolbar);
        consoleButton = findViewById(R.id.buttonConsole);
        startButton = findViewById(R.id.buttonStart);
        shutdownButton = findViewById(R.id.buttonShutdown);
        stopButton = findViewById(R.id.buttonStop);
        notesButton = findViewById(R.id.buttonNotes);
        resetButton = findViewById(R.id.buttonReset);
        backupButton = findViewById(R.id.buttonBackup);
        hardwareDetailButton = findViewById(R.id.buttonHardwareDetailed);
        nameText = findViewById(R.id.labelName);
        nodeText = findViewById(R.id.labelNode);
        idText = findViewById(R.id.labelID);
        uptimeText = findViewById(R.id.labelUptime);
        swapText = findViewById(R.id.labelSwap);
        typeLogo = findViewById(R.id.typeLogo);
        ramText = findViewById(R.id.labelRAM);
        cpuText = findViewById(R.id.labelCPU);
        driveText = findViewById(R.id.labelDrive);
        ramGraph = findViewById(R.id.graphRAM);
        cpuGraph = findViewById(R.id.graphCPU);
        hddGraph = findViewById(R.id.graphHDD);
        statusText = findViewById(R.id.labelStatus);
        networkGraph = findViewById(R.id.graphNetwork);
        squareDriveRead = findViewById(R.id.squareDriveRead);
        squareDriveWrite = findViewById(R.id.squareDriveWrite);
        squareNetworkIn = findViewById(R.id.squareNetIn);
        squareNetworkOut = findViewById(R.id.squareNetOut);
        graphSpinner = findViewById(R.id.spinnerTime);
        avgMaxSpinner = findViewById(R.id.spinnerAvgMax);
        graphLayout = findViewById(R.id.graphLayout);
        swapTableRow = findViewById(R.id.tableRowSwap);

        setSupportActionBar(toolbar);
        setTitle(sessionInfo.nodeName + " - " + sessionInfo.machineID);

        if(sessionInfo.machineType.equals("qemu")) swapTableRow.setVisibility(View.GONE);
        if(sessionInfo.machineType.equals("lxc")) resetButton.setVisibility(View.GONE);

        graphSpinnerValues = ArrayAdapter.createFromResource(this, R.array.spinnerGraph, android.R.layout.simple_spinner_dropdown_item);
        graphSpinner.setAdapter(graphSpinnerValues);
        graphSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(!showLoading){
                    new getConfig().execute();
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
                return;
            }
        });

        avgMaxSpinnerValues = ArrayAdapter.createFromResource(this, R.array.avgMaxSpinner, android.R.layout.simple_spinner_dropdown_item);
        avgMaxSpinner.setAdapter(avgMaxSpinnerValues);
        avgMaxSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(!showLoading){
                    new getConfig().execute();
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
                return;
            }
        });

        consoleButton.setOnClickListener((View v) -> {
            Intent intent = new Intent(getApplicationContext(), de.jonasled.proxdroid.SetConsoleActivity.class);
            startActivity(intent);
        });

        hardwareDetailButton.setOnClickListener((View v) -> {
            Intent intent = new Intent(getApplicationContext(), de.jonasled.proxdroid.ServerHardwareActivity.class);;
            startActivity(intent);
        });

        startButton.setOnClickListener((View v) -> new startStopAction().execute("start"));
        shutdownButton.setOnClickListener((View v) -> new startStopAction().execute("shutdown"));
        stopButton.setOnClickListener((View v) -> new startStopAction().execute("stop"));
        resetButton.setOnClickListener((View v) -> new startStopAction().execute("reset"));
        notesButton.setOnClickListener((View v) -> new getNotes().execute());

        backupButton.setOnClickListener((View v) -> {
            Intent intent = new Intent(getApplicationContext(), de.jonasled.proxdroid.backupActivity.class);
            startActivity(intent);
        });
    }

    @Override
    protected void onResume() {
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                new getConfig().execute();
            }
        }, 0, 10000);
        super.onResume();
    }

    @Override
    protected void onPause() {
        timer.cancel();
        super.onPause();
    }

    private class getNotes extends AsyncTask<String, String, String> {
        private ProgressDialog pd;
        private String description = "";

        @Override
        protected void onPreExecute() {
            HelperFunctions.buildKeystore();
            super.onPreExecute();
            pd = ProgressDialog.show(ServerControlActivity.this, "", getResources().getString(R.string.loading), true,
                    false); // Create and show Progress dialog

        }
        @Override
        protected String doInBackground(String... strings) {
            try {
                URL url = new URL(sessionInfo.baseURL + "api2/json/nodes/" + sessionInfo.nodeName + "/" + sessionInfo.machineType + "/" + sessionInfo.machineID + "/config");
                HttpsURLConnection conn2 = (HttpsURLConnection) url.openConnection();
                conn2.setSSLSocketFactory(HelperFunctions.context.getSocketFactory());
                conn2.setHostnameVerifier(HelperFunctions.hostnameVerifier);
                switch (sessionInfo.loginMethod){
                    case 0:
                        conn2.setRequestProperty("Cookie", "PVEAuthCookie=" + sessionInfo.ticket);//set the auth cookie
                        break;
                    case 1:
                        conn2.addRequestProperty("Authorization", sessionInfo.authString);
                        break;
                }
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn2.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    // Append server response in string
                    sb.append(line + "\n");
                }
                JSONObject resourcesJSON = (JSONObject) (new JSONObject(sb.toString())).get("data");
                try{
                    description = resourcesJSON.getString("description");
                } catch (Exception e) {}


            } catch (IOException e){
                e.printStackTrace();
                return "failedIO";
            } catch (JSONException e){
                e.printStackTrace();
                return "failedJSON";
            }

            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            pd.dismiss();
            if (result == "failedIO") {
                timer.cancel();
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(ServerControlActivity.this);
                dlgAlert.setMessage(R.string.errNotReachable);
                dlgAlert.setTitle(R.string.errNotReachableTitle);
                dlgAlert.setCancelable(false);
                dlgAlert.setPositiveButton(R.string.ok,(DialogInterface dialog, int which) -> finish());
                dlgAlert.create().show();
            } else if (result == "failedJSON") {
                timer.cancel();
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(ServerControlActivity.this);
                dlgAlert.setMessage(R.string.errInvalidJson);
                dlgAlert.setTitle(R.string.errInvalidJsonTitle);
                dlgAlert.setCancelable(false);
                dlgAlert.setPositiveButton(R.string.ok,(DialogInterface dialog, int which) -> finish());
                dlgAlert.create().show();
            }
            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(ServerControlActivity.this);
            dlgAlert.setTitle(R.string.notes);
            dlgAlert.setMessage(description);
            dlgAlert.setPositiveButton(R.string.ok, null);
            dlgAlert.create().show();
        }
    }

    private class startStopAction extends AsyncTask<String, String, String> {
        private ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            HelperFunctions.buildKeystore();
            super.onPreExecute();
            pd = ProgressDialog.show(ServerControlActivity.this, "", getResources().getString(R.string.loading), true,
                    false); // Create and show Progress dialog
            ClusterActivity.updateRequired = true;

        }
        @Override
        protected String doInBackground(String... strings) {
            try {
                String action = strings[0];
                URL url = new URL(sessionInfo.baseURL + "api2/json/nodes/" + sessionInfo.nodeName + "/" + sessionInfo.machineType + "/" + sessionInfo.machineID + "/status/" + action);
                //open the connection and send POST data
                HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                conn.setSSLSocketFactory(HelperFunctions.context.getSocketFactory());
                conn.setHostnameVerifier(HelperFunctions.hostnameVerifier);
                switch (sessionInfo.loginMethod){
                    case 0:
                        conn.setRequestProperty("Cookie", "PVEAuthCookie=" + sessionInfo.ticket);//set the auth cookie
                        conn.setRequestProperty("CSRFPreventionToken", sessionInfo.CSRFPreventionToken);
                        break;
                    case 1:
                        conn.addRequestProperty("Authorization", sessionInfo.authString);
                        break;
                }
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write("");
                wr.flush();

                //get the response from the server
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;

                while ((line = reader.readLine()) != null) {
                    // Append server response in string
                    sb.append(line + "\n");
                }
            } catch (IOException e){
                e.printStackTrace();
                return "failedIO";
            }

            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            pd.dismiss();
            if (result == "failedIO") {
                timer.cancel();
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(ServerControlActivity.this);
                dlgAlert.setMessage(R.string.errNotReachable);
                dlgAlert.setTitle(R.string.errNotReachableTitle);
                dlgAlert.setCancelable(false);
                dlgAlert.setPositiveButton(R.string.ok,(DialogInterface dialog, int which) -> finish());
                dlgAlert.create().show();
            }


            Timer timer2 = new Timer();
            timer2.schedule(new TimerTask() {
                public void run() {
                    new getConfig().execute();
                }
            }, 2000);
        }
    }

    private class getConfig extends AsyncTask<String, String, String> {
        private ProgressDialog pd;
        int spinnerID;
        int spinner2ID;
        JSONObject resourcesJSON_;

        @Override
        protected void onPreExecute() {
            HelperFunctions.buildKeystore();
            ServerControlActivity.this.runOnUiThread(() -> {
                super.onPreExecute();
                if(showLoading) {
                pd = ProgressDialog.show(ServerControlActivity.this, "", getResources().getString(R.string.getDataOf) + " " + sessionInfo.nodeName, true,
                        false); // Create and show Progress dialog
                }
                ramGraphSeries = new ArrayList<Entry>();
                cpuGraphSeries = new  ArrayList<Entry>();
                hddWriteGraphSeries = new ArrayList<Entry>();
                hddReadGraphSeries = new ArrayList<Entry>();
                networkInGraphSeries = new ArrayList<Entry>();
                networkOutGraphSeries = new ArrayList<Entry>();
            });
            spinnerID = graphSpinner.getSelectedItemPosition();
            spinner2ID = avgMaxSpinner.getSelectedItemPosition();

        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                URL url = new URL( sessionInfo.baseURL + "api2/json/cluster/resources");
                HttpsURLConnection conn2 = (HttpsURLConnection) url.openConnection();
                conn2.setSSLSocketFactory(HelperFunctions.context.getSocketFactory());
                conn2.setHostnameVerifier(HelperFunctions.hostnameVerifier);
                switch (sessionInfo.loginMethod){
                    case 0:
                        conn2.setRequestProperty("Cookie", "PVEAuthCookie=" + sessionInfo.ticket);//set the auth cookie
                        break;
                    case 1:
                        conn2.addRequestProperty("Authorization", sessionInfo.authString);
                        break;
                }
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn2.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    // Append server response in string
                    sb.append(line + "\n");
                }
                JSONArray resourcesJSON = (JSONArray) (new JSONObject(sb.toString())).get("data");
                for(int i=0; i < resourcesJSON.length(); i++){
                    JSONObject temp = resourcesJSON.getJSONObject(i);
                    try {
                        if ((temp.getString("node").equals(sessionInfo.nodeName)) && temp.getInt("vmid") == Integer.parseInt(sessionInfo.machineID)) {
                            vmConfig = temp;
                            break;
                        }
                    } catch (Exception e) {}
                }
                url = new URL( sessionInfo.baseURL + "api2/json/nodes/" + sessionInfo.nodeName + "/" + sessionInfo.machineType + "/" + sessionInfo.machineID + "/status/current");
                conn2 = (HttpsURLConnection) url.openConnection();
                conn2.setSSLSocketFactory(HelperFunctions.context.getSocketFactory());
                conn2.setHostnameVerifier(HelperFunctions.hostnameVerifier);
                switch (sessionInfo.loginMethod){
                    case 0:
                        conn2.setRequestProperty("Cookie", "PVEAuthCookie=" + sessionInfo.ticket);//set the auth cookie
                        break;
                    case 1:
                        conn2.addRequestProperty("Authorization", sessionInfo.authString);
                        break;
                }
                reader = new BufferedReader(new InputStreamReader(conn2.getInputStream()));
                sb = new StringBuilder();
                while ((line = reader.readLine()) != null) {
                    // Append server response in string
                    sb.append(line + "\n");
                }
                resourcesJSON_ = (JSONObject) (new JSONObject(sb.toString())).get("data");
                sessionInfo.spiceSupported = (!resourcesJSON_.isNull("spice")) ? (resourcesJSON_.getInt("spice") == 1) : (sessionInfo.machineType.equals("lxc"));

                String timeframe = "hour";
                switch (spinnerID){
                    case 0:
                        timeframe = "hour";
                        break;
                    case 1:
                        timeframe = "day";
                        break;
                    case 2:
                        timeframe = "week";
                        break;
                    case 3:
                        timeframe = "month";
                        break;
                    case 4:
                        timeframe = "year";
                        break;
                }
                String cf = "AVERAGE";
                switch (spinnerID){
                    case 0:
                        cf = "AVERAGE";
                        break;
                    case 1:
                        cf = "MAX";
                        break;
                }

                url = new URL(sessionInfo.baseURL + "api2/json/nodes/" + sessionInfo.nodeName + "/" + sessionInfo.machineType + "/" + sessionInfo.machineID + "/rrddata?timeframe=" + timeframe + "&cf=" + cf);
                conn2 = (HttpsURLConnection) url.openConnection();
                conn2.setSSLSocketFactory(HelperFunctions.context.getSocketFactory());
                conn2.setHostnameVerifier(HelperFunctions.hostnameVerifier);
                switch (sessionInfo.loginMethod){
                    case 0:
                        conn2.setRequestProperty("Cookie", "PVEAuthCookie=" + sessionInfo.ticket);//set the auth cookie
                        break;
                    case 1:
                        conn2.addRequestProperty("Authorization", sessionInfo.authString);
                        break;
                }
                reader = new BufferedReader(new InputStreamReader(conn2.getInputStream()));
                sb = new StringBuilder();
                while ((line = reader.readLine()) != null) {
                    // Append server response in string
                    sb.append(line + "\n");
                }
                JSONArray graphJson = (JSONArray) (new JSONObject(sb.toString())).get("data");
                for(int i=0; i < graphJson.length(); i++) {
                    JSONObject temp = graphJson.getJSONObject(i);
                    try{
                        ramGraphSeries.add(new Entry(temp.getLong("time") * 1000, (float) temp.getDouble("mem")));
                        cpuGraphSeries.add(new Entry(temp.getLong("time") * 1000,(float) temp.getDouble("cpu") * 100));
                        hddWriteGraphSeries.add(new Entry(temp.getLong("time") * 1000, (float) temp.getDouble("diskwrite")));
                        hddReadGraphSeries.add(new Entry(temp.getLong("time") * 1000, (float) temp.getDouble("diskread")));
                        networkInGraphSeries.add(new Entry(temp.getLong("time") * 1000, (float) temp.getDouble("netin")));
                        networkOutGraphSeries.add(new Entry(temp.getLong("time") * 1000, (float) temp.getDouble("netout")));
                    } catch (Exception e) {} //Sometimes one entry is missing

                }




            } catch (IOException e){
                e.printStackTrace();
                return "failedIO";
            } catch (JSONException e){
                e.printStackTrace();
                return "failedJSON";
            } catch (NullPointerException  e){
                e.printStackTrace();
                return "nullPointer";
            }

            return "";

        }

        @Override
        protected void onPostExecute(String result) {
            ServerControlActivity.this.runOnUiThread(() -> {
                boolean graphAnimation = false;
                if (showLoading) {
                    graphAnimation = true;
                    pd.dismiss();
                    showLoading = false;
                }
                if (result == "failedIO") {
                    timer.cancel();
                    AlertDialog.Builder dlgAlert = new AlertDialog.Builder(ServerControlActivity.this);
                    dlgAlert.setMessage(R.string.errNotReachable);
                    dlgAlert.setTitle(R.string.errNotReachableTitle);
                    dlgAlert.setCancelable(false);
                    dlgAlert.setPositiveButton(R.string.ok,(DialogInterface dialog, int which) -> finish());
                    dlgAlert.create().show();
                } else if (result == "nullPointer") {
                    timer.cancel();
                    AlertDialog.Builder dlgAlert = new AlertDialog.Builder(ServerControlActivity.this);
                    dlgAlert.setMessage(R.string.errUnknownLong);
                    dlgAlert.setTitle(R.string.errUnknown);
                    dlgAlert.setCancelable(false);
                    dlgAlert.setPositiveButton(R.string.ok,(DialogInterface dialog, int which) -> finish());
                    dlgAlert.create().show();
                } else if (result == "failedJSON") {
                    timer.cancel();
                    AlertDialog.Builder dlgAlert = new AlertDialog.Builder(ServerControlActivity.this);
                    dlgAlert.setMessage(R.string.errInvalidJson);
                    dlgAlert.setTitle(R.string.errInvalidJsonTitle);
                    dlgAlert.setCancelable(false);
                    dlgAlert.setPositiveButton(R.string.ok,(DialogInterface dialog, int which) -> finish());
                    dlgAlert.create().show();
            }
                try{
                    sessionInfo.machineName = vmConfig.getString("name");
                    if(sessionInfo.machineType.equals("lxc")){
                        typeLogo.setImageResource(R.drawable.lxc_running);
                        swapText.setText(de.jonasled.proxdroid.HelperFunctions.humanReadableByteCountBin(resourcesJSON_.getLong("swap")) + " / " + de.jonasled.proxdroid.HelperFunctions.humanReadableByteCountBin(resourcesJSON_.getLong("maxswap")));
                    } else {
                        typeLogo.setImageResource(R.drawable.qemu_running);
                    }
                    float cpuUsage = Math.round(vmConfig.getDouble("cpu") * 10000);
                    cpuUsage /= 100;


                    setTitle(sessionInfo.nodeName + " - " + sessionInfo.machineName);
                    nameText.setText(sessionInfo.machineName);
                    nodeText.setText(sessionInfo.nodeName);
                    idText.setText(sessionInfo.machineID);
                    ramText.setText(de.jonasled.proxdroid.HelperFunctions.humanReadableByteCountBin(vmConfig.getLong("mem")) + " / " + de.jonasled.proxdroid.HelperFunctions.humanReadableByteCountBin(vmConfig.getLong("maxmem")));
                    cpuText.setText( cpuUsage + "%");
                    if(sessionInfo.machineType.equals("lxc")) {
                        driveText.setText(de.jonasled.proxdroid.HelperFunctions.humanReadableByteCountBin(vmConfig.getLong("disk")) + " / " + de.jonasled.proxdroid.HelperFunctions.humanReadableByteCountBin(vmConfig.getLong("maxdisk")));
                    } else {
                        driveText.setText(de.jonasled.proxdroid.HelperFunctions.humanReadableByteCountBin(vmConfig.getLong("maxdisk")));
                    }
                    if(vmConfig.getString("status").equals("running")){
                        statusText.setText(R.string.running);
                        startButton.setEnabled(false);
                        shutdownButton.setEnabled(true);
                        stopButton.setEnabled(true);
                        consoleButton.setEnabled(true);
                    } else {
                        statusText.setText(R.string.stopped);
                        startButton.setEnabled(true);
                        shutdownButton.setEnabled(false);
                        stopButton.setEnabled(false);
                        consoleButton.setEnabled(false);
                    }

                    int day = (int) TimeUnit.SECONDS.toDays(vmConfig.getLong("uptime"));
                    long hours = TimeUnit.SECONDS.toHours(vmConfig.getLong("uptime")) - (day *24);
                    long minute = TimeUnit.SECONDS.toMinutes(vmConfig.getLong("uptime")) - (TimeUnit.SECONDS.toHours(vmConfig.getLong("uptime"))* 60);
                    long second = TimeUnit.SECONDS.toSeconds(vmConfig.getLong("uptime")) - (TimeUnit.SECONDS.toMinutes(vmConfig.getLong("uptime")) *60);

                    String outputText = day + " " + getResources().getString(R.string.days) + " ";
                    if(hours < 10) outputText += "0";
                    outputText += hours + ":";
                    if(minute < 10) outputText += "0";
                    outputText += minute + ":";
                    if(second < 10) outputText += "0";
                    outputText += second;

                    uptimeText.setText(outputText);

                    graphHandler.setupGraph(ramGraph, graphSpinner.getSelectedItemPosition() <= 1, true, graphAnimation, ServerControlActivity.this, ramGraphSeries);
                    graphHandler.setupGraph(cpuGraph, graphSpinner.getSelectedItemPosition() <= 1, false, graphAnimation, ServerControlActivity.this, cpuGraphSeries);
                    graphHandler.setupGraph(hddGraph, graphSpinner.getSelectedItemPosition() <= 1, true, graphAnimation, ServerControlActivity.this, hddWriteGraphSeries, hddReadGraphSeries);
                    graphHandler.setupGraph(networkGraph, graphSpinner.getSelectedItemPosition() <= 1, true, graphAnimation, ServerControlActivity.this, networkInGraphSeries, networkOutGraphSeries);


                } catch (JSONException e){
                    timer.cancel();
                    e.printStackTrace();
                    AlertDialog.Builder dlgAlert = new AlertDialog.Builder(ServerControlActivity.this);
                    dlgAlert.setMessage(R.string.errInvalidJson);
                    dlgAlert.setTitle(R.string.errInvalidJsonTitle);
                    dlgAlert.setCancelable(false);
                    dlgAlert.setPositiveButton(R.string.ok,(DialogInterface dialog, int which) -> finish());
                    dlgAlert.create().show();
                } catch (Exception e){
                    e.printStackTrace();
                }
            });

        }

    }

}

package de.jonasled.proxdroid;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.appcompat.widget.Toolbar;

import com.google.common.primitives.Ints;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;

import javax.net.ssl.HttpsURLConnection;


public class tasksActivity extends AppCompatActivity {
    ListView listView;
    ArrayList<String> tasks = new ArrayList<>();
    ArrayList<String> taskIDs = new ArrayList<>();
    ArrayList<Integer> tasksIcon = new ArrayList<>();
    ArrayList<Integer> usage = new ArrayList<>();
    de.jonasled.proxdroid.ClusterListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(startActivity.theme == 3) setTheme(R.style.Theme_App_black);
        setContentView(R.layout.activity_tasks);
        Toolbar toolbar = findViewById(R.id.toolbar3);
        setSupportActionBar(toolbar);
        setTitle(getResources().getString(R.string.app_name) + " - " + getResources().getString(R.string.tasks));
        listView = findViewById(R.id.listView);

        new getConfig().execute();

        listView.setOnItemClickListener((AdapterView<?> parent, View view, int position, long id) -> {//wait for click on listView element
            Intent intent = new Intent(getApplicationContext(), viewTaskLog.class); //start the Server control activity with some parameters
            intent.putExtra("taskName", taskIDs.get(position));
            startActivity(intent);
        });

    }

    private class getConfig extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            HelperFunctions.buildKeystore();
            tasks = new ArrayList<>();
            tasksIcon = new ArrayList<>();
            usage = new ArrayList<>();
            taskIDs = new ArrayList<>();
        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                URL url = new URL( sessionInfo.baseURL + "api2/json/cluster/tasks");
                HttpsURLConnection conn2 = (HttpsURLConnection) url.openConnection();
                conn2.setSSLSocketFactory(HelperFunctions.context.getSocketFactory());
                conn2.setHostnameVerifier(HelperFunctions.hostnameVerifier);
                switch (sessionInfo.loginMethod){
                    case 0:
                        conn2.setRequestProperty("Cookie", "PVEAuthCookie=" + sessionInfo.ticket);//set the auth cookie
                        conn2.setRequestProperty("CSRFPreventionToken", sessionInfo.CSRFPreventionToken);
                        break;
                    case 1:
                        conn2.addRequestProperty("Authorization", sessionInfo.authString);
                        break;
                }
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn2.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    // Append server response in string
                    sb.append(line).append("\n");
                }
                JSONArray resourcesJSONUnsorted = (JSONArray) (new JSONObject(sb.toString())).get("data");
                JSONArray resourcesJSON = HelperFunctions.sortJson(resourcesJSONUnsorted, "starttime");



                for(int i=resourcesJSON.length(); i > 0; i--){
                    JSONObject temp = resourcesJSON.getJSONObject(i - 1);
                    try {
                        usage.add(-1);
                        if(temp.has("status")) {
                            if (temp.getString("status").equals("OK")) {
                                tasksIcon.add(R.drawable.icons8_ok_240);
                            } else {
                                tasksIcon.add(R.drawable.icons8_cancel_480);
                            }
                        } else {
                            tasksIcon.add(R.drawable.icons8_clock_240);
                        }

                        String type = temp.getString("type");
                        String user = temp.getString("user");
                        String id = temp.getString("id");
                        String node = temp.getString("node");

                        if(type.equals("vncproxy") || type.equals("spiceproxy")){
                            tasks.add(getResources().getString(R.string.console) + ": " + id + " (" + user + ")");
                        } else if(type.equals("qmstart") || type.equals("vzstart")){
                            tasks.add(getResources().getString(R.string.start) + ": " + id + " (" + user + ")");
                        } else if(type.equals("qmshutdown") || type.equals("vzshutdown")){
                            tasks.add(getResources().getString(R.string.shutdown) + ": " + id + " (" + user + ")");
                        } else if(type.equals("qmstop") || type.equals("vzstop")){
                            tasks.add(getResources().getString(R.string.stop) + ": " + id + " (" + user + ")");
                        } else if(type.equals("qmreboot") || type.equals("vzreboot")){
                            tasks.add(getResources().getString(R.string.reboot) + ": " + id + " (" + user + ")");
                        } else if(type.equals("vzdump")){
                            tasks.add(getResources().getString(R.string.backup) + ": " + id + " (" + user + ")");
                        } else if(type.equals("aptupdate")){
                            tasks.add(getResources().getString(R.string.aptUpdate));
                        } else if(type.equals("auth-realm-sync")){
                            tasks.add(getResources().getString(R.string.realmSync) + ": " + id);
                            tasks.add(getResources().getString(R.string.aptUpdate) + ": " + node);
                        } else if(type.equals("startall")){
                            tasks.add(getResources().getString(R.string.startAll) + ": " + node);
                        } else if(type.equals("stopall")){
                            tasks.add(getResources().getString(R.string.stopAll) + ": " + node);
                        } else if(type.equals("clusterjoin")){
                            tasks.add(getResources().getString(R.string.joinCluster) + ": " + node);
                        }else if(type.equals("vzcreate") || type.equals("qmcreate")){
                            tasks.add(getResources().getString(R.string.create) + ": " + node + " - " + id);
                        } else if(type.equals("vzdestroy") || type.equals("qmdestroy")){
                            tasks.add(getResources().getString(R.string.destroy) + ": " + node + " - " + id);
                        } else if(type.equals("qmmove") || type.equals("vzmove")){
                            tasks.add(getResources().getString(R.string.moveDisk) + ": " + node + " - " + id);
                        } else if(type.equals("acmerenew")){
                            tasks.add(getResources().getString(R.string.certRenew) + ": " + node);
                        } else {
                            tasks.add(temp.getString("upid"));
                        }
                        taskIDs.add(temp.getString("upid"));


                    } catch (Exception e) {e.printStackTrace();}
                }


            } catch (IOException e){
                e.printStackTrace();
                return "failedIO";
            } catch (JSONException e){
                e.printStackTrace();
                return "failedJSON";
            }

            return "";

        }

        @Override
        protected void onPostExecute(String result) {
            int[] iconInt = Ints.toArray(tasksIcon);
            int[] usageInt = Ints.toArray(usage);
            String[] taskArray = tasks.toArray(new String[0]);//convert the List to array and show the listView
            adapter = new de.jonasled.proxdroid.ClusterListAdapter(tasksActivity.this, taskArray, iconInt, usageInt);
            listView.setAdapter(adapter);
        }

    }
}
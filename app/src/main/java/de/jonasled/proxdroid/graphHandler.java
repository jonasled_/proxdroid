package de.jonasled.proxdroid;

import android.content.Context;
import android.graphics.Color;
import androidx.core.content.ContextCompat;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.google.android.material.resources.TextAppearance;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.floor;
import static java.lang.Math.round;

public class graphHandler {

    private static int[] colors = new int[] {
            Color.BLUE,
            Color.RED
    };

    public static void setupGraph(LineChart graph, boolean timeAsHours, boolean yAxisIsSize, boolean animate, Context context, List<Entry>... series){
        int textColor = Color.parseColor("#" + Integer.toHexString(context.getResources().getColor(R.color.graphLegend)));

        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        for (int i = 0; i<series.length; i++){
            List<Entry> series_ = series[i];
            int color = colors[i % colors.length];

            LineDataSet dataSet = new LineDataSet(series_, "");
            dataSet.setDrawValues(false);
            dataSet.setDrawCircles(false);
            dataSet.setAxisDependency(YAxis.AxisDependency.LEFT);
            dataSet.setColor(color);
            dataSet.setLineWidth(2f);
            dataSet.setFillAlpha(65);
            dataSet.setFillColor(color);
            LineData lineData = new LineData(dataSet);
            dataSets.add(dataSet);
        }

        LineData data = new LineData(dataSets);
        graph.setData(data);
        graph.getDescription().setEnabled(false);
        graph.setDrawGridBackground(false);
        graph.setTouchEnabled(true);
        graph.setDragEnabled(true);
        graph.setScaleEnabled(true);
        Legend l = graph.getLegend();
        l.setEnabled(false);
        graph.getAxisRight().setEnabled(false);

        YAxis yAxis = graph.getAxisLeft();
        yAxis.setLabelCount(3);
        yAxis.setTextColor(textColor);
        yAxis.setTextSize(14);
        yAxis.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                if(yAxisIsSize){
                    return HelperFunctions.humanReadableByteCountBin((long) value);
                } else {
                    return ((float) round(value * 100) / 100) + " %";
                }
            }
        });

        XAxis xAxis = graph.getXAxis();
        xAxis.setLabelCount(4);
        xAxis.setTextColor(textColor);
        xAxis.setTextSize(14);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                String timeString;
                if(timeAsHours){
                    timeString = (new SimpleDateFormat("HH:mm")).format(value);
                } else {
                    timeString = (new SimpleDateFormat("MM-dd")).format(value);
                }
                return timeString;
            }
        });

        if(animate){
            graph.animateX(500);
        }
        graph.invalidate(); // refresh
    }
}

package de.jonasled.proxdroid;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import com.google.common.primitives.Ints;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.preference.PreferenceManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.URL;
import java.nio.Buffer;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLHandshakeException;

public class ClusterActivity extends AppCompatActivity {

    public static boolean updateRequired = false;
    int databaseID;
    private de.jonasled.proxdroid.DBHelper mydb;
    JSONObject login;
    String username, password, totpToken, postContent, realm;
    ArrayList<String> nodes = new ArrayList<>();
    ArrayList<String> label = new ArrayList<>();
    ArrayList<Integer> icon = new ArrayList<>();
    ArrayList<Integer> storageUsage = new ArrayList<>();
    de.jonasled.proxdroid.ClusterListAdapter adapter;
    ListView listView;
    List sortedKeys;
    Map<String, ArrayList<String>> vmctList = new HashMap<>();
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            if(startActivity.theme == 3) setTheme(R.style.Theme_App_black);
            setContentView(R.layout.activity_cluster);
            listView = findViewById(R.id.listView);
            swipeRefreshLayout = findViewById(R.id.swiperefresh);
            mydb = new de.jonasled.proxdroid.DBHelper(this);

            Toolbar toolbar = findViewById(R.id.toolbar);//find the UI elements
            setSupportActionBar(toolbar);

            Cursor cursor = mydb.getData(sessionInfo.databaseID);
            cursor.moveToFirst();

            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    sessionInfo.baseURL = HelperFunctions.decryptMsg(Base64.getDecoder().decode(cursor.getString(2)));
                    username = HelperFunctions.decryptMsg(Base64.getDecoder().decode(cursor.getString(4)));
                    password = HelperFunctions.decryptMsg(Base64.getDecoder().decode(cursor.getString(5)));
                    totpToken = HelperFunctions.decryptMsg(Base64.getDecoder().decode(cursor.getString(7)));
                } else {
                    sessionInfo.baseURL = HelperFunctions.decryptMsg(android.util.Base64.decode(cursor.getString(2), android.util.Base64.DEFAULT));
                    username = HelperFunctions.decryptMsg(android.util.Base64.decode(cursor.getString(4), android.util.Base64.DEFAULT));
                    password = HelperFunctions.decryptMsg(android.util.Base64.decode(cursor.getString(5), android.util.Base64.DEFAULT));
                    totpToken = HelperFunctions.decryptMsg(android.util.Base64.decode(cursor.getString(7), android.util.Base64.DEFAULT));
                }

                realm = cursor.getString(3);
                sessionInfo.loginMethod = cursor.getInt(8);

                sessionInfo.hostname = sessionInfo.baseURL.split("//")[1].split("/")[0];
                if(sessionInfo.hostname.contains(":")){
                    sessionInfo.hostname = sessionInfo.hostname.split(":")[0];
                }
                sessionInfo.authString = "PVEAPIToken=" + username + "=" + password;

                new loginAsync().execute();//login to server
            } catch (Exception e) {
                e.printStackTrace();
            }

            listView.setOnItemClickListener((AdapterView<?> parent, View view, int position, long id) -> {//wait for click on listView element
                Intent intent = null;
                if((vmctList.get(sortedKeys.get(position)).get(3).equals("lxc")) || (vmctList.get(sortedKeys.get(position)).get(3).equals("qemu"))) {
                    intent = new Intent(getApplicationContext(), ServerControlActivity.class); //start the Server control activity with some parameters
                } else if(vmctList.get(sortedKeys.get(position)).get(3).equals("node")){
                    intent = new Intent(getApplicationContext(), NodeControlActivity.class); //start the Server control activity with some parameters
                } else if(vmctList.get(sortedKeys.get(position)).get(3).equals("storage")){
                    intent = new Intent(getApplicationContext(), StorageControlActivity.class); //start the Server control activity with some parameters
                }

                sessionInfo.machineID = vmctList.get(sortedKeys.get(position)).get(1);//the id of the machine e.g. 100
                sessionInfo.nodeName = vmctList.get(sortedKeys.get(position)).get(2);//the name of the node e.g. pve
                sessionInfo.machineType = vmctList.get(sortedKeys.get(position)).get(3);//the type of the VM (qemu or LXC)
                startActivity(intent);
            });

            swipeRefreshLayout.setOnRefreshListener(() -> {
                try {
                    new loginAsync().execute(sessionInfo.baseURL, postContent, totpToken);//login to server
                } catch (Exception e) {
                    e.printStackTrace();
                }

                swipeRefreshLayout.setRefreshing(false);
            });

        } catch (Exception e){
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            String sStackTrace = sw.toString(); // stack trace as a string
            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(ClusterActivity.this);
            dlgAlert.setMessage(sStackTrace);
            dlgAlert.setTitle(R.string.errUnknown);
            dlgAlert.setCancelable(false);
            dlgAlert.setPositiveButton(R.string.ok, (DialogInterface dialog, int which) -> {});
            dlgAlert.create().show();

        }



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.cluster_tasks_button, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.open_tasks) {
            Intent intent = new Intent(getApplicationContext(), tasksActivity.class); //start the Server control activity with some parameters
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        if (updateRequired) {
            updateRequired = false;
            try {
                new loginAsync().execute(sessionInfo.baseURL, postContent, totpToken);//login to server
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        super.onResume();
    }

    private class loginAsync extends AsyncTask<String, String, String> {//login task
        private ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = ProgressDialog.show(ClusterActivity.this, "", getResources().getString(R.string.loading), true,//open a loading dialog
                    false);
        }


        @Override
        protected String doInBackground(String... strings){

            //pd.setMessage(getResources().getString(R.string.login));
            try {
                HelperFunctions.buildKeystore();

                if(sessionInfo.loginMethod == 0){
                    URL url = new URL(sessionInfo.baseURL + "api2/json/access/ticket");//parse the URL

                    //open the connection and send POST data
                    HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                    conn.setSSLSocketFactory(HelperFunctions.context.getSocketFactory());//our SSL handler
                    conn.setHostnameVerifier(HelperFunctions.hostnameVerifier);
                    conn.setDoOutput(true);
                    OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());//write the POST data
                    wr.write("username=" + HelperFunctions.encodeValue(username) + "@" + HelperFunctions.encodeValue(realm) + "&password=" + HelperFunctions.encodeValue(password));
                    wr.flush();

                    //get the response from the server
                    BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        // Append server response in string
                        sb.append(line + "\n");
                    }

                    login = (JSONObject) (new JSONObject(sb.toString())).get("data");//parse the JSON
                    sessionInfo.ticket = login.getString("ticket");//get the ticket
                    sessionInfo.CSRFPreventionToken = login.getString("CSRFPreventionToken");//get the CSRFPreventionToken
                    if (sb.toString().contains("NeedTFA")) {
                        url = new URL(sessionInfo.baseURL + "api2/extjs/access/tfa"); //parse the URL
                        //open the connection and send POST data
                        conn = (HttpsURLConnection) url.openConnection();
                        conn.setSSLSocketFactory(HelperFunctions.context.getSocketFactory());//set our SSL handler
                        conn.setHostnameVerifier(HelperFunctions.hostnameVerifier);
                        conn.setRequestProperty("CSRFPreventionToken", sessionInfo.CSRFPreventionToken);//set the CSRFPreventionToken
                        conn.setRequestProperty("Cookie", "PVEAuthCookie=" +  sessionInfo.ticket);//set the auth cookie
                        conn.setDoOutput(true);
                        wr = new OutputStreamWriter(conn.getOutputStream());
                        wr.write("response=" + HelperFunctions.totpGenerator(totpToken));//send the 2FA token
                        wr.flush();

                        //get the response from the server
                        reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                        sb = new StringBuilder();
                        while ((line = reader.readLine()) != null) {
                            // Append server response in string
                            sb.append(line + "\n");
                        }
                        login = (JSONObject) (new JSONObject(sb.toString())).get("data");//parse the JSON
                        sessionInfo.ticket = login.getString("ticket");//get the new Ticket
                    }
                }

                //pd.setMessage(getResources().getString(R.string.getNode));
                URL url = new URL(sessionInfo.baseURL + "api2/json/nodes");//parse the URL
                HttpsURLConnection conn2 = (HttpsURLConnection) url.openConnection(); //open the URL with our custom SSL handler
                conn2.setHostnameVerifier(HelperFunctions.hostnameVerifier);
                conn2.setSSLSocketFactory(HelperFunctions.context.getSocketFactory());
                switch (sessionInfo.loginMethod){
                    case 0:
                        conn2.setRequestProperty("Cookie", "PVEAuthCookie=" + sessionInfo.ticket);//set the auth cookie
                        break;
                    case 1:
                        conn2.addRequestProperty("Authorization", sessionInfo.authString);
                        break;
                }
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn2.getInputStream()));//download and parse the Response
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    // Append server response in string
                    sb.append(line + "\n");
                }

                JSONArray nodesJSON = (JSONArray) (new JSONObject(sb.toString())).get("data");//parse the response as JSON
                for(int i=0; i < nodesJSON.length(); i++){//scan all nodes and add them to the nodes array
                    nodes.add(nodesJSON.getJSONObject(i).getString("node"));
                }

                for (String node:nodes) {//get for all nodes the VMs and container
                    try {
                        //pd.setMessage(getResources().getString(R.string.getVM_CT) + " " + node);
                        url = new URL(sessionInfo.baseURL + "api2/json/nodes/" + node + "/lxc");//get the LXC container
                        conn2 = (HttpsURLConnection) url.openConnection();
                        conn2.setHostnameVerifier(HelperFunctions.hostnameVerifier);
                        conn2.setSSLSocketFactory(HelperFunctions.context.getSocketFactory());//set our SSL handler
                        switch (sessionInfo.loginMethod) {
                            case 0:
                                conn2.setRequestProperty("Cookie", "PVEAuthCookie=" + sessionInfo.ticket);//set the auth cookie
                                break;
                            case 1:
                                conn2.addRequestProperty("Authorization", sessionInfo.authString);
                                break;
                        }
                        reader = new BufferedReader(new InputStreamReader(conn2.getInputStream()));//read the response
                        sb = new StringBuilder();
                        while ((line = reader.readLine()) != null) {
                            // Append server response in string
                            sb.append(line + "\n");
                        }
                        JSONArray containerJSON = (JSONArray) (new JSONObject(sb.toString())).get("data");//parse the response as JSON
                        for (int i = 0; i < containerJSON.length(); i++) {//parse every LXC container
                            ArrayList<String> temp = new ArrayList<>();//make a array for the container values
                            temp.add(containerJSON.getJSONObject(i).getString("name"));//get the container name and add it
                            temp.add(containerJSON.getJSONObject(i).getString("vmid"));//get the container ID and add it
                            temp.add(node); //add the node name
                            temp.add("lxc");//add the type
                            temp.add(containerJSON.getJSONObject(i).getString("status"));//add the curent status (online, offline)
                            vmctList.put(containerJSON.getJSONObject(i).getString("vmid") + "/" + node, temp);//add this array to the vm dict withe the id and the node as key

                        }
                        url = new URL(sessionInfo.baseURL + "api2/json/nodes/" + node + "/qemu");//do the same as for LXC, but for qemu VMs
                        conn2 = (HttpsURLConnection) url.openConnection();
                        conn2.setHostnameVerifier(HelperFunctions.hostnameVerifier);
                        conn2.setSSLSocketFactory(HelperFunctions.context.getSocketFactory());//our custom SSL handler
                        switch (sessionInfo.loginMethod) {
                            case 0:
                                conn2.setRequestProperty("Cookie", "PVEAuthCookie=" + sessionInfo.ticket);//set the auth cookie
                                break;
                            case 1:
                                conn2.addRequestProperty("Authorization", sessionInfo.authString);
                                break;
                        }
                        reader = new BufferedReader(new InputStreamReader(conn2.getInputStream()));//wait for the data to arrive
                        sb = new StringBuilder();
                        while ((line = reader.readLine()) != null) {
                            // Append server response in string
                            sb.append(line + "\n");
                        }
                        containerJSON = (JSONArray) (new JSONObject(sb.toString())).get("data");//parse the data as JSON
                        for (int i = 0; i < containerJSON.length(); i++) {//parse the single VMs
                            ArrayList<String> temp = new ArrayList<>();//make a array for the VM values
                            temp.add(containerJSON.getJSONObject(i).getString("name"));//get the VM name and add it
                            temp.add(containerJSON.getJSONObject(i).getString("vmid"));//get the VM ID and add it
                            temp.add(node);//add the node name
                            temp.add("qemu");//add the type
                            temp.add(containerJSON.getJSONObject(i).getString("status"));//add the curent status (online, offline)
                            vmctList.put(containerJSON.getJSONObject(i).getString("vmid") + "/" + node, temp);//add this array to the vm dict withe the id and the node as key
                        }
                    } catch (FileNotFoundException e) {
                        // this happens when the node is offline
                        e.printStackTrace();
                    }
                }

                url = new URL(sessionInfo.baseURL + "api2/json/cluster/resources");//parse the URL
                conn2 = (HttpsURLConnection) url.openConnection(); //open the URL with our custom SSL handler
                conn2.setHostnameVerifier(HelperFunctions.hostnameVerifier);
                conn2.setSSLSocketFactory(HelperFunctions.context.getSocketFactory());
                switch (sessionInfo.loginMethod){
                    case 0:
                        conn2.setRequestProperty("Cookie", "PVEAuthCookie=" + sessionInfo.ticket);//set the auth cookie
                        break;
                    case 1:
                        conn2.addRequestProperty("Authorization", sessionInfo.authString);
                        break;
                }
                reader = new BufferedReader(new InputStreamReader(conn2.getInputStream()));//download and parse the Response
                sb = new StringBuilder();
                while ((line = reader.readLine()) != null) {
                    // Append server response in string
                    sb.append(line + "\n");
                }

                JSONArray resourcesJSON = (JSONArray) (new JSONObject(sb.toString())).get("data");//parse the response as JSON
                for(int i=0; i < resourcesJSON.length(); i++){//scan all nodes and add them to the nodes array
                    JSONObject resource = resourcesJSON.getJSONObject(i);
                    if(resource.getString("type").equals("node")){
                        ArrayList<String> temp = new ArrayList<>();//make a array for the VM values
                        temp.add(resource.getString("node"));//get the VM name and add it
                        temp.add(resource.getString("id"));//get the VM ID and add it
                        temp.add(resource.getString("node"));//add the node name
                        temp.add("node");//add the type
                        temp.add(resource.getString("status"));//add the curent status (online, offline)
                        vmctList.put(resource.getString("id"), temp);//add this array to the vm dict withe the id and the node as key
                    }
                    if(resource.getString("type").equals("storage")){
                        ArrayList<String> temp = new ArrayList<>();//make a array for the VM values
                        temp.add(resource.getString("storage"));//get the VM name and add it
                        temp.add(resource.getString("id"));//get the VM ID and add it
                        temp.add(resource.getString("node"));//add the node name
                        temp.add("storage");//add the type
                        temp.add(resource.getString("status"));//add the curent status (online, offline)
                        if(resource.getString("status").equals("available")) {
                            try {
                                temp.add("" + Math.round(resource.getDouble("disk") / resource.getDouble("maxdisk") * 100));
                            } catch (JSONException e){
                                temp.add("");
                            }
                        } else {
                            temp.add("");
                        }
                        vmctList.put(resource.getString("id"), temp);//add this array to the vm dict withe the id and the node as key
                    }
                }




                return "";

            } catch (SSLHandshakeException e){//handle some exceptions
                e.printStackTrace();
                HelperFunctions.trustCert(ClusterActivity.this, sessionInfo.baseURL + "/");
                return "";
            } catch (IOException e){
                e.printStackTrace();
                e.printStackTrace();
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                e.printStackTrace(pw);
                String sStackTrace = sw.toString(); // stack trace as a string
                return "failedIO|" + sStackTrace;
            } catch (JSONException e){
                e.printStackTrace();
                e.printStackTrace();
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                e.printStackTrace(pw);
                String sStackTrace = sw.toString(); // stack trace as a string
                return "failedJSON|" + sStackTrace;
            } catch (IllegalArgumentException e){
                e.printStackTrace();
                e.printStackTrace();
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                e.printStackTrace(pw);
                String sStackTrace = sw.toString(); // stack trace as a string
                return "failedJSON|" + sStackTrace;
            } catch (Exception e){
                e.printStackTrace();
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                e.printStackTrace(pw);
                String sStackTrace = sw.toString(); // stack trace as a string
                return sStackTrace;
            }


        }


        @Override
        protected void onPostExecute(String result) {
            pd.dismiss();//close the loading dialog
            AlertDialog.Builder dlgAlert2 = new AlertDialog.Builder(ClusterActivity.this);
            if(result != ""){//if a error occurred, build the dialog for the detailed info
                dlgAlert2.setMessage(result.substring(result.indexOf("|") + 1));
                dlgAlert2.setTitle(R.string.detailedError);
                dlgAlert2.setCancelable(false);
                dlgAlert2.setPositiveButton(R.string.ok, (DialogInterface dialog, int which) -> finish());
            }

            if (result.startsWith("failedIO")) {//handle the exceptions from above
                SharedPreferences mPreferences = PreferenceManager.getDefaultSharedPreferences(ClusterActivity.this);
                if(mPreferences.getBoolean("callIntent", false) && !HelperFunctions.lastConnectName.equals(sessionInfo.baseURL)) {
                    PackageManager pm = getApplicationContext().getPackageManager();
                    Intent intent = pm.getLaunchIntentForPackage(mPreferences.getString("intentApp", ""));
                    if (intent != null){
                        intent.setAction(mPreferences.getString("callIntentAction", ""));
                        String[] extras = mPreferences.getString("callIntentExtra", "").split("\n");
                        for (String extra:extras) {
                            String extraName = extra.split(":", 2)[0];
                            String extraValue = extra.split(":", 2)[1];
                            if(extraValue.equals("true")) intent.putExtra(extraName, true);
                            else if(extraValue.equals("false")) intent.putExtra(extraName, false);
                            else intent.putExtra(extraName, false);
                        }
                        HelperFunctions.lastConnectName = sessionInfo.baseURL;
                        startActivity(intent);
                        finish();
                    }
                }
                else if(!mPreferences.getBoolean("callIntent", false) || HelperFunctions.lastConnectName.equals(sessionInfo.baseURL)) {

                    AlertDialog.Builder dlgAlert = new AlertDialog.Builder(ClusterActivity.this);
                    dlgAlert.setMessage(R.string.errNotReachable);
                    dlgAlert.setTitle(R.string.errNotReachableTitle);
                    dlgAlert.setPositiveButton(R.string.ok, (DialogInterface dialog, int which) -> finish());
                    dlgAlert.setNegativeButton(R.string.moreDetails, (DialogInterface dialog, int which) -> dlgAlert2.show());
                    dlgAlert.create().show();
                }
            } else if (result.startsWith("failedJSON")) {
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(ClusterActivity.this);
                dlgAlert.setMessage(R.string.errInvalidJson);
                dlgAlert.setTitle(R.string.errInvalidJsonTitle);
                dlgAlert.setCancelable(false);
                dlgAlert.setPositiveButton(R.string.ok, (DialogInterface dialog, int which) -> finish());
                dlgAlert.setNegativeButton(R.string.moreDetails, (DialogInterface dialog, int which) -> dlgAlert2.show());
                dlgAlert.create().show();
            } else if(result != "") {
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(ClusterActivity.this);
                dlgAlert.setMessage(result);
                dlgAlert.setTitle(R.string.errUnknown);
                dlgAlert.setCancelable(false);
                dlgAlert.setPositiveButton(R.string.ok, (DialogInterface dialog, int which) -> finish());
                dlgAlert.setNegativeButton(R.string.moreDetails, (DialogInterface dialog, int which) -> dlgAlert2.show());
                dlgAlert.create().show();
                return;
            }

            Map<String, ArrayList<String>> temp = new HashMap<>();
            label.clear(); //clear the label array
            icon.clear(); //clear the icon array
            sortedKeys = new ArrayList(vmctList.keySet());//The response from proxmox is a mess, no System or whatever, so we sort the array after the key
            Collections.sort(sortedKeys);

            SharedPreferences mPreferences = PreferenceManager.getDefaultSharedPreferences(ClusterActivity.this);


            int lastindex = 0; //get the last container / VM in list
            for (int i = 0; i < sortedKeys.size(); i++) {
                Object key = sortedKeys.get(i);
                if ((vmctList.get(key.toString()).get(3) != "lxc") && (vmctList.get(key.toString()).get(3) != "qemu")) {
                    lastindex = i;
                    break;
                }
            }
            for (int i = 0; i < lastindex; i++) { //sort the container / VMs alphabetical
                for (int j = i + 1; j < lastindex; j++) {
                    if(mPreferences.getString("sortOrder", "0").equals("1")) {
                        if (vmctList.get(sortedKeys.get(i)).get(0).toLowerCase().compareTo(vmctList.get(sortedKeys.get(j)).get(0).toLowerCase()) > 0) {
                            Object temp_ = sortedKeys.get(i);
                            sortedKeys.set(i, sortedKeys.get(j));
                            sortedKeys.set(j, temp_);
                        }
                    } else {
                        try {
                            if (Integer.valueOf(vmctList.get(sortedKeys.get(i)).get(1)) > Integer.valueOf(vmctList.get(sortedKeys.get(j)).get(1))) {
                                Object temp_ = sortedKeys.get(i);
                                sortedKeys.set(i, sortedKeys.get(j));
                                sortedKeys.set(j, temp_);
                            }
                        } catch (Exception e) {}
                    }
                }
            }
            int startindex = lastindex;
            for (int i = lastindex; i < sortedKeys.size(); i++) { //get the last node in list
                Object key = sortedKeys.get(i);
                if ((vmctList.get(key.toString()).get(3) != "node")) {
                    lastindex = i;
                    break;
                }
            }
            for (int i = startindex; i < lastindex; i++) { //sort the nodes alphabetical
                for (int j = i + 1; j < lastindex; j++) {
                    if (vmctList.get(sortedKeys.get(i)).get(0).toLowerCase().compareTo(vmctList.get(sortedKeys.get(j)).get(0).toLowerCase()) > 0) {
                        Object temp_ = sortedKeys.get(i);
                        sortedKeys.set(i, sortedKeys.get(j));
                        sortedKeys.set(j, temp_);
                    }
                }
            }
            startindex = lastindex;
            for (int i = startindex; i < sortedKeys.size(); i++) { //sort the storages alphabetical
                for (int j = i + 1; j < sortedKeys.size(); j++) {
                    if (vmctList.get(sortedKeys.get(i)).get(0).toLowerCase().compareTo(vmctList.get(sortedKeys.get(j)).get(0).toLowerCase()) > 0) {
                        Object temp_ = sortedKeys.get(i);
                        sortedKeys.set(i, sortedKeys.get(j));
                        sortedKeys.set(j, temp_);
                    }
                }
            }



            for (Object key : sortedKeys.toArray()) {//parse the sorted key list
                temp.put(key.toString(), vmctList.get(key.toString()));//make a new sorted array

                label.add(vmctList.get(key.toString()).get(0) + " (" + key + ")");//add the label for the VM to the label array

                if (vmctList.get(key.toString()).get(3) == "lxc") {//check if vm is qemu or LXC and if online or not
                    storageUsage.add(-1);
                    if (vmctList.get(key.toString()).get(4).equals("running")) {
                        icon.add(R.drawable.lxc_running);//lxc normal color
                    } else {
                        icon.add(R.drawable.lxc_offline);//lxc grey and white
                    }
                } else if (vmctList.get(key.toString()).get(3) == "qemu") {
                    storageUsage.add(-1);
                    if (vmctList.get(key.toString()).get(4).equals("running")) {
                        icon.add(R.drawable.qemu_running);//qemu color
                    } else {
                        icon.add(R.drawable.qemu_offline);//qemu grey and white
                    }
                } else if (vmctList.get(key.toString()).get(3) == "node") {
                    storageUsage.add(-1);
                    label.set(label.size() - 1, vmctList.get(key.toString()).get(0));
                    if (vmctList.get(key.toString()).get(4).equals("online")) {
                        icon.add(R.drawable.server_online);//server color
                    } else {
                        icon.add(R.drawable.server_offline);//server grey and white
                    }
                } else if (vmctList.get(key.toString()).get(3) == "storage") {
                    label.set(label.size() - 1,vmctList.get(key.toString()).get(0) + " (" + vmctList.get(key.toString()).get(2) + ")");
                    if (vmctList.get(key.toString()).get(4).equals("available")) {
                        icon.add(R.drawable.storage_online);//storage color
                        storageUsage.add((int) Integer.parseInt(vmctList.get(key.toString()).get(5)));
                    } else {
                        icon.add(R.drawable.storage_offline);//storage grey and white
                        storageUsage.add(-1);
                    }
                }
            }
            vmctList = temp;

            int[] iconsInt = Ints.toArray(icon);
            int[] usageInt = Ints.toArray(storageUsage);
            String[] labelArray = label.toArray(new String[0]);//convert the List to array and show the listView
            adapter = new de.jonasled.proxdroid.ClusterListAdapter(ClusterActivity.this, labelArray, iconsInt, usageInt);
            listView.setAdapter(adapter);
        }
    }

}

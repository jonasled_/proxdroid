package de.jonasled.proxdroid;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.preference.PreferenceManager;
import com.google.common.primitives.Ints;
import org.json.JSONException;
import org.json.JSONObject;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class SetConsoleActivity extends AppCompatActivity {

    PackageManager pm;
    ListView listView;
    ArrayList<String> appID;
    String spiceURL = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_console);
        this.setFinishOnTouchOutside(false);
        listView = findViewById(R.id.listView);

        spiceURL = sessionInfo.baseURL + "api2/extjs/nodes/" + sessionInfo.nodeName + "/" + sessionInfo.machineType  + "/" + sessionInfo.machineID  + "/spiceproxy";

        ArrayList<String> title = new ArrayList<>();
        ArrayList<Integer> icon = new ArrayList<>();
        ArrayList<Integer> storageUsage = new ArrayList<>();

        title.add("noVNC");
        icon.add(R.drawable.novnc);
        storageUsage.add(-1);

        if(sessionInfo.spiceSupported) {
            title.add("SPICE");
            icon.add(R.drawable.virt_viewer);
            storageUsage.add(-1);
        }

        if(sessionInfo.machineType.equals("lxc")){
            title.add("XTERM.JS");
            icon.add(R.drawable.xterm);
            storageUsage.add(-1);
        }

        if(!sessionInfo.spiceSupported && !sessionInfo.machineType.equals("lxc")){
            Intent intent = new Intent(getApplicationContext(), de.jonasled.proxdroid.ConsoleView.class);
            String type = sessionInfo.machineType;
            if(type.equals("qemu")) type = "kvm";

            intent.putExtra("url", sessionInfo.baseURL + "?console=" + type + "&vmid=" + sessionInfo.machineID  + "&node=" + sessionInfo.nodeName + "&novnc=1");
            startActivity(intent);
            finish();
        }




        de.jonasled.proxdroid.ClusterListAdapter adapter;
        int[] iconsInt = Ints.toArray(icon);
        int[] usageInt = Ints.toArray(storageUsage);
        String[] labelArray = title.toArray(new String[0]);//convert the List to array and show the listView
        adapter = new de.jonasled.proxdroid.ClusterListAdapter(SetConsoleActivity.this, labelArray, iconsInt, usageInt);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener((AdapterView<?> parent, View view, int position, long id) -> {//wait for click on listView element
            if(position == 0){
                Intent intent = new Intent(getApplicationContext(), de.jonasled.proxdroid.ConsoleView.class);
                String type = sessionInfo.machineType;
                if(type.equals("qemu")) type = "kvm";

                intent.putExtra("url", sessionInfo.baseURL + "?console=" + type + "&vmid=" + sessionInfo.machineID  + "&node=" + sessionInfo.nodeName + "&novnc=1");
                startActivity(intent);
                finish();
            }
            if(position == 1){
                new startSpice().execute();
                finish();
            }
            if(position == 2){
                Intent intent = new Intent(getApplicationContext(), de.jonasled.proxdroid.ConsoleView.class);
                String type = sessionInfo.machineType;
                if(type.equals("qemu")) type = "kvm";

                intent.putExtra("url", sessionInfo.baseURL + "?console=" + type + "&vmid=" + sessionInfo.machineID  + "&node=" + sessionInfo.nodeName + "&xtermjs=1");
                startActivity(intent);
                finish();
            }
        });
    }

    private class startSpice extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            HelperFunctions.buildKeystore();
            super.onPreExecute();

        }
        @Override
        protected String doInBackground(String... strings) {
            try {
                URL url = new URL(spiceURL);
                //open the connection and send POST data
                HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                conn.setSSLSocketFactory(HelperFunctions.context.getSocketFactory());
                conn.setHostnameVerifier(HelperFunctions.hostnameVerifier);
                conn.setRequestProperty("Cookie", "PVEAuthCookie=" + sessionInfo.ticket);//set the auth cookie
                conn.setRequestProperty("CSRFPreventionToken", sessionInfo.CSRFPreventionToken);
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write("proxy=" + sessionInfo.hostname);
                wr.flush();

                //get the response from the server
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;

                while ((line = reader.readLine()) != null) {
                    // Append server response in string
                    sb.append(line + "\n");
                }
                JSONObject json = (JSONObject) (new JSONObject(sb.toString())).get("data");
                String spiceFile="[virt-viewer]\n";
                spiceFile += "host-subject=" + json.getString("host-subject") + "\n";
                spiceFile += "ca=" + json.getString("ca") + "\n";
                spiceFile += "release-cursor=" + json.getString("release-cursor") + "\n";
                spiceFile += "proxy=" + json.getString("proxy") + "\n";
                spiceFile += "secure-attention=" + json.getString("secure-attention") + "\n";
                spiceFile += "delete-this-file=" + json.getInt("delete-this-file") + "\n";
                spiceFile += "toggle-fullscreen=" + json.getString("toggle-fullscreen") + "\n";
                spiceFile += "host=" + json.getString("host") + "\n";
                spiceFile += "title=" + json.getString("title") + "\n";
                spiceFile += "password=" + json.getString("password") + "\n";
                spiceFile += "type=" + json.getString("type") + "\n";
                spiceFile += "tls-port=" + json.getString("tls-port");
                return spiceFile;


            } catch (IOException e){
                e.printStackTrace();
                return "failedIO";
            } catch (JSONException e){
                e.printStackTrace();
                return "failedJSON";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if (result == "failedIO") {
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(SetConsoleActivity.this);
                dlgAlert.setMessage(R.string.errNotReachable);
                dlgAlert.setTitle(R.string.errNotReachableTitle);
                dlgAlert.setCancelable(false);
                dlgAlert.setPositiveButton(R.string.ok,(DialogInterface dialog, int which) -> {});
                dlgAlert.create().show();
            } else if (result == "failedJSON") {
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(SetConsoleActivity.this);
                dlgAlert.setMessage(R.string.errInvalidJson);
                dlgAlert.setTitle(R.string.errInvalidJsonTitle);
                dlgAlert.setCancelable(false);
                dlgAlert.setPositiveButton(R.string.ok,(DialogInterface dialog, int which) -> {});
                dlgAlert.create().show();
            }
            File file;
            try {
                file = new File(SetConsoleActivity.this.getCacheDir(), "spice");
                //file = File.createTempFile("spice", null, ServerControlActivity.this.getCacheDir());
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(file));
                outputStreamWriter.write(result);
                outputStreamWriter.close();



                Intent intent = new Intent();
                Uri apkURI = FileProvider.getUriForFile(
                        SetConsoleActivity.this,
                        SetConsoleActivity.this.getApplicationContext()
                                .getPackageName() + ".provider", file);
                intent.setDataAndType(apkURI, "application/x-virt-viewer");
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.setAction(android.content.Intent.ACTION_VIEW);
                if(intent.resolveActivity(getPackageManager()) != null){
                    startActivity(intent);
                } else {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(SetConsoleActivity.this);
                    builder1.setMessage(R.string.spiceClientRequired);
                    builder1.setCancelable(true);
                    builder1.setPositiveButton(
                            R.string.ok,
                            (dialog, id) -> {});
                    builder1.setNeutralButton(
                            R.string.showOpaque,
                            (dialog, id) -> {
                                try {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.undatech.opaque")));
                                } catch (android.content.ActivityNotFoundException anfe) {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.undatech.opaque")));
                                }
                            }
                            );

                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
package de.jonasled.proxdroid;

import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import android.annotation.SuppressLint;
import android.app.VoiceInteractor;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.graphics.Bitmap;
import android.net.http.SslCertificate;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

import java.lang.reflect.Field;
import java.security.cert.X509Certificate;

import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class ConsoleView extends AppCompatActivity {

    WebView browser;
    String url;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_console_view);
        browser = findViewById(R.id.browser);//search the browser in layout
        url = getIntent().getStringExtra("url");
        SharedPreferences mPreferences = PreferenceManager.getDefaultSharedPreferences(ConsoleView.this); //get the SettingsActivity

        //fullscreen the view
        getSupportActionBar().hide();
        if(mPreferences.getBoolean("consoleFullscreen", true)){//hide the status bar, if enabled in SettingsActivity
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);
        }

        CookieManager.getInstance().setCookie(sessionInfo.baseURL, "PVEAuthCookie=" + sessionInfo.ticket);//set the auth cookie

        browser.setWebViewClient(new customWebViewClient());//setup the WebView
        WebChromeClientCustom mWebChromeClient = new WebChromeClientCustom();
        browser.setWebChromeClient(mWebChromeClient);

        browser.getSettings().setJavaScriptEnabled(true);//enable some SettingsActivity in chrome WebView
        browser.getSettings().setDomStorageEnabled(true);
        browser.getSettings().setUseWideViewPort(true);
        browser.getSettings().setLoadWithOverviewMode(true);
        browser.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

        if (0 != (getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE)) {//if build is debugActivity, enabled webView remote debugging (on pc open chrome://inspect/#devices)
            WebView.setWebContentsDebuggingEnabled(true);
        }

        browser.loadUrl(url);

    }
    private class customWebViewClient extends WebViewClient { //modified web view client with custom SSL handling

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            boolean passVerify = false;

            if((error.getPrimaryError() == SslError.SSL_UNTRUSTED) || (error.getPrimaryError() == SslError.SSL_IDMISMATCH)){ //check if SSL certificate is untrusted
                SslCertificate cert = error.getCertificate(); //get Certificate
                try{
                    Field f = cert.getClass().getDeclaredField("mX509Certificate");
                    f.setAccessible(true);
                    X509Certificate x509 = (X509Certificate)f.get(cert);
                    passVerify = HelperFunctions.keyStore.isCertificateEntry(x509.getSerialNumber().toString());
                }catch(Exception e){e.printStackTrace();}
            }
            if(passVerify){
                handler.proceed();
            } else{
                handler.cancel();
            }

        }

        @Override
        public void onPageFinished(WebView view, String url){
            SharedPreferences mPreferences = PreferenceManager.getDefaultSharedPreferences(ConsoleView.this);//get the SettingsActivity module
            if(url.contains("xterm")){//check if we have a xterm console, if yes inject custom font size via javascript
                view.loadUrl("javascript: (function() {term.setOption('fontSize', " + mPreferences.getString("xtermSize", "15") + ");})()");
            } else if(url.contains("novnc")){//check if console is novnc
                view.loadUrl("javascript: (function() {document.getElementById(\"noVNC_fullscreen_button\").click();})()");//press the fullscreen button in the menue on the right
            }
        }

    }

    private class WebChromeClientCustom extends WebChromeClient { //taken from https://medium.com/@oliverdamjan2013/fullscreen-youtube-video-in-webview-android-21f1c778cf56 (we need this to get fullscreen working)
        private View mCustomView;
        private WebChromeClient.CustomViewCallback mCustomViewCallback;
        private int mOriginalOrientation;
        private int mOriginalSystemUiVisibility;
        public void onHideCustomView() {
            ((FrameLayout) getWindow().getDecorView()).removeView(this.mCustomView);
            this.mCustomView = null;
            getWindow().getDecorView().setSystemUiVisibility(this.mOriginalSystemUiVisibility);
            setRequestedOrientation(this.mOriginalOrientation);
            this.mCustomViewCallback.onCustomViewHidden();
            this.mCustomViewCallback = null;
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER);
        }
        @Override
        public void onShowCustomView(View paramView, WebChromeClient.CustomViewCallback paramCustomViewCallback) {
            if (this.mCustomView != null) {
                onHideCustomView();
                return;
            }
            this.mCustomView = paramView;
            this.mOriginalSystemUiVisibility = getWindow().getDecorView().getSystemUiVisibility();
            this.mOriginalOrientation = getRequestedOrientation();
            this.mCustomViewCallback = paramCustomViewCallback;
            ((FrameLayout) getWindow()
                    .getDecorView())
                    .addView(this.mCustomView, new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            ConsoleView.this.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN |
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                    View.SYSTEM_UI_FLAG_IMMERSIVE);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER);
            this.mCustomView.setOnSystemUiVisibilityChangeListener(visibility -> updateControls());
        }
        @Override
        public Bitmap getDefaultVideoPoster() {
            return Bitmap.createBitmap(10, 10, Bitmap.Config.ARGB_8888);
        }
        void updateControls() {
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) this.mCustomView.getLayoutParams();
            params.bottomMargin = 0;
            params.topMargin = 0;
            params.leftMargin = 0;
            params.rightMargin = 0;
            params.height = ViewGroup.LayoutParams.MATCH_PARENT;
            params.width = ViewGroup.LayoutParams.MATCH_PARENT;
            this.mCustomView.setLayoutParams(params);
            ConsoleView

                    .this.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN |
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                    View.SYSTEM_UI_FLAG_IMMERSIVE);
        }
    }
}

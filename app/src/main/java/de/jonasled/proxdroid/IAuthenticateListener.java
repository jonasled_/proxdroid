package de.jonasled.proxdroid;

public interface IAuthenticateListener {

    void onAuthenticate(String decryptPassword);
}
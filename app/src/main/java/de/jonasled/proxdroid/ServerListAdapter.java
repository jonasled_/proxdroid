package de.jonasled.proxdroid;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.preference.PreferenceManager;

import javax.annotation.Nullable;

public class ServerListAdapter extends BaseAdapter {
    private String title[];
    private int image;
    private LayoutInflater layoutInflater;
    private Context context;


    public ServerListAdapter(Context applicationContext, String[] title, int image) {
        this.title = title;
        this.image = image;
        layoutInflater = (LayoutInflater.from(applicationContext));
        context = applicationContext;
    }

    @Override
    public int getCount() {
        try{
            return title.length;
        }catch (Exception e){
            return 0;
        }
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        int size = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(context).getString("textSize", "30")); //get the text size
        view = layoutInflater.inflate(R.layout.activity_listview, null);

        TextView titleTV =  view.findViewById(R.id.title);
        ImageView imageIV =  view.findViewById(R.id.image);
        titleTV.setText(title[i]);
        imageIV.setImageResource(image);
        ProgressBar storageUsage = view.findViewById(R.id.usageBar);

        storageUsage.setVisibility(View.GONE);

        titleTV.setTextSize(size);//set the text size
        imageIV.getLayoutParams().width = (int) (size * context.getResources().getDisplayMetrics().density);//set the image size, according to the text size

        return view;
    }
}
package de.jonasled.proxdroid;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.hardware.fingerprint.FingerprintManagerCompat;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;

import java.util.Base64;

public class SettingsActivity extends AppCompatActivity {

    private static SharedPreferences mPreferences;
    private static SharedPreferences.Editor editor;
    //noinspection deprecation
    private static FingerprintManagerCompat fingerprintManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(startActivity.theme == 3) setTheme(R.style.Theme_App_black);
        setContentView(R.layout.settings_activity);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.settings, new SettingsFragment())
                .commit();
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        mPreferences = PreferenceManager.getDefaultSharedPreferences(SettingsActivity.this);
        editor = mPreferences.edit();
        //noinspection deprecation
        fingerprintManager = FingerprintManagerCompat.from(SettingsActivity.this);

    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }


    public static class SettingsFragment extends PreferenceFragmentCompat {
        int clickcount=0;
        long lastClick = 0;

        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey);


            findPreference("fingerprintEnabled").setEnabled(fingerprintManager.isHardwareDetected() && fingerprintManager.hasEnrolledFingerprints() && (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O));
            findPreference("advancedSettingsCategory").setVisible(mPreferences.getBoolean("showAdvancedSettings", false));

            findPreference("fingerprintEnabled").setOnPreferenceChangeListener((Preference preference, Object parameter) -> {
                if(!mPreferences.getBoolean("fingerprintEnabled", false)) {
                    editor.remove("decryptKey");

                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) { //can't happen on lower android versions. Its just for hiding the error
                        editor.putString("decryptKey", Base64.getEncoder().encodeToString(startActivity.decryptToken.getEncoded()));
                    }
                }
                editor.commit();
                return true;
            });


            findPreference("joinBeta").setOnPreferenceClickListener((Preference preference) -> {//call a browser with the beta testing invite link, on button press
                Uri uri = Uri.parse("https://play.google.com/apps/testing/de.jonasled.proxdroid");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                return true;
            });
            findPreference("bugFound").setOnPreferenceClickListener((Preference preference) -> {//open the preferred email application with already prefilled the feedback email, on button press
                Uri uri = Uri.parse("mailto:" + getResources().getString(R.string.feedbackMail));
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                return true;
            });
            findPreference("donate").setOnPreferenceClickListener((Preference preference) -> {//call a browser with my paypal donate link, on button press
                Uri uri = Uri.parse("https://paypal.me/jonasled");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                return true;
            });
            findPreference("sourcecode").setOnPreferenceClickListener((Preference preference) -> {//call a browser with the gitlab link, on button press
                Uri uri = Uri.parse("https://gitlab.com/jonasled_/proxdroid");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                return true;
            });
            findPreference("discord").setOnPreferenceClickListener((Preference preference) -> {//call a browser with the discord invite link, on button press
                Uri uri = Uri.parse("https://discord.gg/JEuXW8h");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                return true;
            });
            findPreference("version").setOnPreferenceClickListener((Preference preference) -> {//this function is used to call the debug activity, if user pressed 5 times the version in 10 secconds
                if(lastClick + 10 < System.currentTimeMillis() / 1000L){
                    clickcount = 0;
                }
                lastClick = System.currentTimeMillis() / 1000L;
                clickcount++;
                if(clickcount >= 5){
                    clickcount = 0;
                    Intent intent = new Intent(this.getContext(), debugActivity.class);
                    startActivity(intent);
                }
                return true;
            });
            findPreference("translate").setOnPreferenceClickListener((Preference preference) -> {
                AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this.getContext());
                dlgAlert.setMessage(R.string.translatorsDescription);
                dlgAlert.setTitle(R.string.translators);
                dlgAlert.setPositiveButton("OK", null);
                dlgAlert.setCancelable(true);
                dlgAlert.create().show();
                return true;
            });
            findPreference("showAdvancedSettings").setOnPreferenceChangeListener((Preference preference, Object parameter) -> {
                findPreference("advancedSettingsCategory").setVisible(!mPreferences.getBoolean("showAdvancedSettings", true)); //the action is executed, before the setting is updated
                return true;
            });
            findPreference("selectIntentApp").setOnPreferenceClickListener((Preference preference) -> {
                Intent intent = new Intent(this.getContext(), SelectIntentApp.class);
                startActivity(intent);
                return true;
            });
            findPreference("testIntent").setOnPreferenceClickListener((Preference preference) -> {
                PackageManager pm = this.getContext().getPackageManager();
                Intent intent = pm.getLaunchIntentForPackage(mPreferences.getString("intentApp", ""));
                if (intent != null){
                    intent.setAction(mPreferences.getString("callIntentAction", ""));
                    String[] extras = mPreferences.getString("callIntentExtra", "").split("\n");
                    for (String extra:extras) {
                        String extraName = extra.split(":", 2)[0];
                        String extraValue = extra.split(":", 2)[1];
                        if(extraValue.equals("true")) intent.putExtra(extraName, true);
                        else if(extraValue.equals("false")) intent.putExtra(extraName, false);
                        else intent.putExtra(extraName, false);
                    }
                    startActivity(intent);
                }
                return true;
            });
        }

    }
}
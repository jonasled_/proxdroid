package de.jonasled.proxdroid;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import android.widget.*;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.text.TextUtils;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Base64;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLHandshakeException;

public class NewServer extends AppCompatActivity {
    TextView inputName;
    TextView inputURL;
    TextView inputUser;
    TextView inputPassword;
    TextView inputTOTP;
    TextView inputRealm;
    TextView labelRealm;
    TextView labelUsername;
    TextView labelPassword;
    CheckBox checkBoxRequiresTOTP;
    Spinner spinnerLoginType;
    de.jonasled.proxdroid.DBHelper mydb;
    FloatingActionButton fab;
    Button buttonTestLogin;
    boolean edit;
    ArrayAdapter<String> realmSelect;
    ArrayList<String> temp = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(startActivity.theme == 3) setTheme(R.style.Theme_App_black);
        setContentView(R.layout.activity_new_server);
        Toolbar toolbar = findViewById(R.id.toolbar);//find the UI elements
        setSupportActionBar(toolbar);
        inputName = findViewById(R.id.inputName);
        inputURL = findViewById(R.id.inputURL);
        inputRealm = findViewById(R.id.inputRealm);
        inputUser = findViewById(R.id.inputUser);
        inputPassword = findViewById(R.id.inputPassword);
        buttonTestLogin = findViewById(R.id.buttonTestLogin);
        checkBoxRequiresTOTP = findViewById(R.id.checkBoxTOTP);
        labelRealm = findViewById(R.id.textViewRealm);
        labelUsername = findViewById(R.id.textViewUsername);
        labelPassword = findViewById(R.id.textViewPassword);
        fab = findViewById(R.id.fab);
        inputTOTP = findViewById(R.id.inputTOTP);
        spinnerLoginType =findViewById(R.id.spinnerLoginType);
        mydb = new de.jonasled.proxdroid.DBHelper(this);

        if(sessionInfo.databaseID == -1){//if database ID is -1, its a new entry
            edit = false;
        } else { //if not get the data from the database and set it to the fields
            edit = true;
            Cursor dbEntry = mydb.getData(sessionInfo.databaseID);
            dbEntry.moveToFirst();
            try {
                inputName.setText(dbEntry.getString(1));
                spinnerLoginType.setSelection(dbEntry.getInt(8));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    inputURL.setText(HelperFunctions.decryptMsg(Base64.getDecoder().decode(dbEntry.getString(2))));
                    inputUser.setText(HelperFunctions.decryptMsg(Base64.getDecoder().decode(dbEntry.getString(4))));
                    inputPassword.setText(HelperFunctions.decryptMsg(Base64.getDecoder().decode(dbEntry.getString(5))));
                    inputRealm.setText(dbEntry.getString(3));
                    checkBoxRequiresTOTP.setChecked(dbEntry.getInt(6) > 0);
                    if (dbEntry.getInt(6) > 0) {
                        inputTOTP.setText(HelperFunctions.decryptMsg(Base64.getDecoder().decode(dbEntry.getString(7))));
                        inputTOTP.setEnabled(true);
                    }
                } else {
                    inputURL.setText(HelperFunctions.decryptMsg(android.util.Base64.decode(dbEntry.getString(2), android.util.Base64.DEFAULT)));
                    inputUser.setText(HelperFunctions.decryptMsg(android.util.Base64.decode(dbEntry.getString(4), android.util.Base64.DEFAULT)));
                    inputPassword.setText(HelperFunctions.decryptMsg(android.util.Base64.decode(dbEntry.getString(5), android.util.Base64.DEFAULT)));
                    inputRealm.setText(dbEntry.getString(3));
                    checkBoxRequiresTOTP.setChecked(dbEntry.getInt(6) > 0);
                    if (dbEntry.getInt(6) > 0) {
                        inputTOTP.setText(HelperFunctions.decryptMsg(android.util.Base64.decode(dbEntry.getString(7), android.util.Base64.DEFAULT)));
                        inputTOTP.setEnabled(true);
                    }
                }
            } catch (Exception e) {e.printStackTrace();}

        }

        spinnerLoginType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int pos, long id) {
                switch (pos){
                    case 0:
                        inputRealm.setVisibility(View.VISIBLE);
                        labelRealm.setVisibility(View.VISIBLE);
                        checkBoxRequiresTOTP.setVisibility(View.VISIBLE);
                        inputTOTP.setVisibility(View.VISIBLE);
                        labelUsername.setText(R.string.username);
                        inputUser.setHint(R.string.username);
                        labelPassword.setText(R.string.password);
                        inputPassword.setHint(R.string.password);
                        break;
                    case 1:
                        inputRealm.setVisibility(View.GONE);
                        labelRealm.setVisibility(View.GONE);
                        checkBoxRequiresTOTP.setVisibility(View.GONE);
                        inputTOTP.setVisibility(View.GONE);
                        labelUsername.setText(R.string.tokenID);
                        inputUser.setHint(R.string.tokenID);
                        labelPassword.setText(R.string.secret);
                        inputPassword.setHint(R.string.secret);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                //Nothing to do here

            }
        });

        inputURL.setOnFocusChangeListener((View v, boolean hasFocus) ->{//on leave focus of the url input, replace https with HTTPS and add / at the end if missing (to fix some problems with android URL handling)
            try {
                if (!inputURL.getText().toString().substring(0, 4).toLowerCase().equals("http")) {
                    inputURL.setText("HTTPS://" + inputURL.getText().toString());
                }
            } catch (IndexOutOfBoundsException e) {}
            inputURL.setText(inputURL.getText().toString().replace("https:", "HTTPS:"));
            inputURL.setText(inputURL.getText().toString().replace("http:", "HTTP:"));
            try {
                if (inputURL.getText().toString().charAt(inputURL.getText().toString().length() - 1) != '/') {
                    if(inputURL.getText().toString().replace("HTTPS:", "").replace("HTTP:", "").contains(":")){
                        inputURL.setText(inputURL.getText().toString() + "/");
                    } else {
                        inputURL.setText(inputURL.getText().toString() + ":8006/");
                    }
                }
            } catch (IndexOutOfBoundsException e) {}
        });

        checkBoxRequiresTOTP.setOnCheckedChangeListener((CompoundButton compoundButton, boolean checked) -> inputTOTP.setEnabled(checked));//if the user enables 2FA, enable the 2FA token input

        fab.setOnClickListener((View v) -> {//save button
            try {
                if (!inputURL.getText().toString().substring(0, 4).toLowerCase().equals("http")) {
                    inputURL.setText("HTTP://" + inputURL.getText().toString());
                }
            } catch (IndexOutOfBoundsException e) {}
            inputURL.setText(inputURL.getText().toString().replace("https:", "HTTPS:"));
            inputURL.setText(inputURL.getText().toString().replace("http:", "HTTP:"));
            try {
                if (inputURL.getText().toString().charAt(inputURL.getText().toString().length() - 1) != '/') {
                    if(inputURL.getText().toString().contains(":")){
                    inputURL.setText(inputURL.getText().toString() + "/");
                } else {
                    inputURL.setText(inputURL.getText().toString() + ":8006/");
                }
                }
            } catch (IndexOutOfBoundsException e) {}

            //clear all errors
            inputName.setError(null);
            inputURL.setError(null);
            inputUser.setError(null);
            inputPassword.setError(null);
            inputTOTP.setError(null);
            inputRealm.setError(null);

            //check if every box is filled, if not add an error and cancel the execution
            if(TextUtils.isEmpty(inputName.getText().toString())){
                inputName.setError(getResources().getString(R.string.errnotEmpty));
                return;
            }
            if(TextUtils.isEmpty(inputURL.getText().toString())){
                inputURL.setError(getResources().getString(R.string.errnotEmpty));
                return;
            }
            if(TextUtils.isEmpty(inputUser.getText().toString())){
                inputUser.setError(getResources().getString(R.string.errnotEmpty));
                return;
            }
            if(TextUtils.isEmpty(inputPassword.getText().toString())){
                inputPassword.setError(getResources().getString(R.string.errnotEmpty));
                return;
            }
            if(TextUtils.isEmpty(inputRealm.getText().toString())){
                inputRealm.setError(getResources().getString(R.string.errnotEmpty));
                return;
            }
            if((TextUtils.isEmpty(inputTOTP.getText().toString())) && checkBoxRequiresTOTP.isChecked()){
                inputTOTP.setError(getResources().getString(R.string.errnotEmpty));
                return;
            }

            //Form Validated, we can proceed
            if(edit){//if we edit an entry, remove the old one
                mydb.deleteServer(sessionInfo.databaseID);
            }
            String totpToken = "";
            if(checkBoxRequiresTOTP.isChecked()) totpToken = inputTOTP.getText().toString(); //if totp is enabled, get the token and save it to variable, if not use an empty String
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    totpToken = Base64.getEncoder().encodeToString(HelperFunctions.encryptMsg(totpToken));
                    String url = Base64.getEncoder().encodeToString(HelperFunctions.encryptMsg(inputURL.getText().toString()));
                    String username = Base64.getEncoder().encodeToString(HelperFunctions.encryptMsg(inputUser.getText().toString()));
                    String password = Base64.getEncoder().encodeToString(HelperFunctions.encryptMsg(inputPassword.getText().toString()));
                    int loginType = spinnerLoginType.getSelectedItemPosition();
                    mydb.insertServer(inputName.getText().toString(), url, inputRealm.getText().toString(), username, password, checkBoxRequiresTOTP.isChecked(), totpToken, loginType); //make the new Database entry
                } else {
                    totpToken = android.util.Base64.encodeToString(HelperFunctions.encryptMsg(totpToken), android.util.Base64.DEFAULT);
                    String url = android.util.Base64.encodeToString(HelperFunctions.encryptMsg(inputURL.getText().toString()), android.util.Base64.DEFAULT);
                    String username = android.util.Base64.encodeToString(HelperFunctions.encryptMsg(inputUser.getText().toString()), android.util.Base64.DEFAULT);
                    String password = android.util.Base64.encodeToString(HelperFunctions.encryptMsg(inputPassword.getText().toString()), android.util.Base64.DEFAULT);
                    int loginType = spinnerLoginType.getSelectedItemPosition();
                    mydb.insertServer(inputName.getText().toString(), url, inputRealm.getText().toString(), username, password, checkBoxRequiresTOTP.isChecked(), totpToken, loginType); //make the new Database entry
                }
            } catch (Exception e) {
                e.printStackTrace();}
            Intent intent = new Intent(getApplicationContext(), ServerActivity.class); //start the main activity
            finish();
            startActivity(intent);
        });
        buttonTestLogin.setOnClickListener((View v) -> {//test if the login credentials are correct
            inputURL.setText(inputURL.getText().toString().replace("https", "HTTPS"));
            try {
                if (inputURL.getText().toString().charAt(inputURL.getText().toString().length() - 1) != '/') {
                    if(inputURL.getText().toString().contains(":")){
                        inputURL.setText(inputURL.getText().toString() + "/");
                    } else {
                        inputURL.setText(inputURL.getText().toString() + ":8006/");
                    }
                }
            } catch (IndexOutOfBoundsException e) {}
            //clear all errors
            inputName.setError(null);
            inputURL.setError(null);
            inputUser.setError(null);
            inputPassword.setError(null);
            inputTOTP.setError(null);

            //check if every box is filled, if not add an error and cancel the execution
            if(TextUtils.isEmpty(inputName.getText().toString())){
                inputName.setError(getResources().getString(R.string.errnotEmpty));
                return;
            }
            if(TextUtils.isEmpty(inputURL.getText().toString())){
                inputURL.setError(getResources().getString(R.string.errnotEmpty));
                return;
            }
            if(TextUtils.isEmpty(inputUser.getText().toString())){
                inputUser.setError(getResources().getString(R.string.errnotEmpty));
                return;
            }
            if(TextUtils.isEmpty(inputPassword.getText().toString())){
                inputPassword.setError(getResources().getString(R.string.errnotEmpty));
                return;
            }
            if((TextUtils.isEmpty(inputTOTP.getText().toString())) && checkBoxRequiresTOTP.isChecked()){
                inputTOTP.setError(getResources().getString(R.string.errnotEmpty));
                return;
            }
            if(TextUtils.isEmpty(inputRealm.getText().toString())){
                inputRealm.setError(getResources().getString(R.string.errnotEmpty));
                return;
            }

            sessionInfo.hostname = inputURL.getText().toString().split("//")[1].split("/")[0];
            if(sessionInfo.hostname.contains(":")){
                sessionInfo.hostname = sessionInfo.hostname.split(":")[0];
            }
            new testLogin().execute();
        });
    }
    private class testLogin extends AsyncTask<String, String, String> {
        private ProgressDialog pd;
        String baseurl;
        String login;
        String user;
        String password;
        String realm;
        String totpToken = "";
        int loginMethod;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = ProgressDialog.show(NewServer.this, "", getResources().getString(R.string.login), true,
                    false); // Create and show Progress dialog
            HelperFunctions.buildKeystore();
            baseurl = inputURL.getText().toString();
            user = inputUser.getText().toString();
            realm = inputRealm.getText().toString();
            password = inputPassword.getText().toString();
            if(checkBoxRequiresTOTP.isChecked())totpToken = inputTOTP.getText().toString();
            loginMethod = spinnerLoginType.getSelectedItemPosition();
        }

        @Override
        protected String doInBackground(String... strings){
            try{
                switch(loginMethod) {
                    case 0:
                        URL url = new URL(baseurl + "api2/json/access/ticket"); //prepare the URL
                        //open the connection and send POST data
                        HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                        conn.setSSLSocketFactory(HelperFunctions.context.getSocketFactory());//our custom SSL handler
                        conn.setHostnameVerifier(HelperFunctions.hostnameVerifier);
                        conn.setDoOutput(true);//enable write data to POST
                        OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                        wr.write("username=" + inputUser.getText().toString()  + "@" +  inputRealm.getText().toString() + "&password=" +  inputPassword.getText().toString());//write the POST data
                        wr.flush();//flush the output buffer

                        //get the response from the server
                        BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                        StringBuilder sb = new StringBuilder();
                        String line;

                        while ((line = reader.readLine()) != null) {
                            // Append server response in string
                            sb.append(line + "\n");
                        }


                        JSONObject login = (JSONObject) (new JSONObject(sb.toString())).get("data");//Parse the response as JSON
                        String ticket = login.getString("ticket");//get the login ticket
                        String CSRFPreventionToken = login.getString("CSRFPreventionToken");//get the CSRFPreventionToken (needed for post requests)
                        if (sb.toString().contains("NeedTFA")) { //check if the account needs 2Fa authentication

                            url = new URL(baseurl + "/api2/extjs/access/tfa");//if yes prepare the 2FA url
                            //open the connection and send POST data
                            conn = (HttpsURLConnection) url.openConnection();
                            conn.setHostnameVerifier(HelperFunctions.hostnameVerifier);
                            conn.setSSLSocketFactory(HelperFunctions.context.getSocketFactory());
                            conn.setRequestProperty("CSRFPreventionToken", CSRFPreventionToken);//send our token and ticket to authenticate
                            conn.setRequestProperty("Cookie", "PVEAuthCookie=" + ticket);
                            conn.setDoOutput(true);
                            wr = new OutputStreamWriter(conn.getOutputStream());//write the output
                            wr.write("response=" + HelperFunctions.totpGenerator(totpToken));//generate the token with the helper function
                            wr.flush();

                            //get the response from the server
                            reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                            sb = new StringBuilder();
                            while ((line = reader.readLine()) != null) {
                                // Append server response in string
                                sb.append(line + "\n");
                            }
                            login = (JSONObject) (new JSONObject(sb.toString())).get("data");//parse the response json
                            ticket = login.getString("ticket"); //overwrite the ticket
                        }

                        return ticket;
                    case 1:
                        url = new URL(baseurl + "api2/json/nodes");//parse the URL
                        HttpsURLConnection conn2 = (HttpsURLConnection) url.openConnection(); //open the URL with our custom SSL handler
                        conn2.setHostnameVerifier(HelperFunctions.hostnameVerifier);
                        conn2.setSSLSocketFactory(HelperFunctions.context.getSocketFactory());
                        conn2.addRequestProperty("Authorization", "PVEAPIToken=" + user + "=" + password);
                        reader = new BufferedReader(new InputStreamReader(conn2.getInputStream()));//download and parse the Response
                        sb = new StringBuilder();
                        while ((line = reader.readLine()) != null) {
                            // Append server response in string
                            sb.append(line + "\n");
                        }
                        JSONArray nodesJSON = (JSONArray) (new JSONObject(sb.toString())).get("data");//parse the response as JSON
                        return "";
                }

            } catch (SSLHandshakeException e){//catch some errors
                HelperFunctions.trustCert(NewServer.this, baseurl + "/");//ask to trust the cert
                return "failedSSL";
            } catch (IOException e){
                e.printStackTrace();
                return "failedIO";
            } catch (JSONException | IllegalArgumentException e){
                e.printStackTrace();
                return "failedJSON";
            }


            return "";
        }


        @Override
        protected void onPostExecute(String result) {
            pd.dismiss();
            if (result == "failedIO"){//parse the response from the script before
                AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(NewServer.this);
                dlgAlert.setMessage(R.string.errNotReachable);
                dlgAlert.setTitle(R.string.errNotReachableTitle);
                dlgAlert.setPositiveButton(R.string.ok,(DialogInterface dialog, int which) -> {});
                dlgAlert.create().show();
            } else if(result == "failedJSON") {
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(NewServer.this);
                dlgAlert.setMessage(R.string.errInvalidJson);
                dlgAlert.setTitle(R.string.errInvalidJsonTitle);
                dlgAlert.setCancelable(false);
                dlgAlert.setPositiveButton(R.string.ok, (DialogInterface dialog, int which) -> {
                });
                dlgAlert.create().show();
            } else if (result == "failedSSL"){
            } else {
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(NewServer.this);
                dlgAlert.setMessage(R.string.loginSuccess);
                dlgAlert.setTitle(R.string.loginSuccessTitle);
                dlgAlert.setCancelable(false);
                dlgAlert.setPositiveButton(R.string.ok, (DialogInterface dialog, int which) -> {
                });
                dlgAlert.create().show();
            }

        }

    }

}



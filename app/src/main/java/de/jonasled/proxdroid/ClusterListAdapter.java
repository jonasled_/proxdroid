package de.jonasled.proxdroid;

import android.content.Context;
import android.content.res.Resources;
import android.text.Layout;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.core.content.res.TypedArrayUtils;
import androidx.preference.Preference;
import androidx.preference.PreferenceManager;

import javax.annotation.Nullable;

public class ClusterListAdapter extends BaseAdapter { //This is used to make the list of your VMs and Container
    private String title[];
    private int image[];
    private int usage[];
    Context context;

    LayoutInflater inflter;

    public ClusterListAdapter(Context applicationContext, String[] title, int[] image, int[] usage) {
        this.title = title;
        this.image = image;
        this.usage = usage;

        inflter = (LayoutInflater.from(applicationContext));
        context = applicationContext;
    }

    @Override
    public int getCount() {
        return title.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        int size = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(context).getString("textSize", "30"));//get the text size

        view = inflter.inflate(R.layout.activity_listview, null); //set the view to a new entry
        TextView titleTV =  view.findViewById(R.id.title);//find the textView
        ImageView imageIV =  view.findViewById(R.id.image);//find the ImageView
        ProgressBar storageUsage = view.findViewById(R.id.usageBar);

        if (usage[i] == -1) {
            storageUsage.setVisibility(View.GONE);
        } else {
            storageUsage.setProgress(usage[i]);
        }

        titleTV.setText(title[i]);//set the title
        try{
            imageIV.setImageResource(image[i]);//set the image (Proxmox or LXC logo)
        } catch (Resources.NotFoundException e){
            imageIV.setImageResource(R.drawable.no_image);
        }

        titleTV.setTextSize(size);//set the text size
        imageIV.getLayoutParams().width = (int) (size * context.getResources().getDisplayMetrics().density);//set the image size, according to the text size

        return view;//set the new view
    }
}
package de.jonasled.proxdroid;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.ContextMenu;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class ServerActivity extends AppCompatActivity {

    private de.jonasled.proxdroid.DBHelper mydb;
    ArrayList<String> databaseID;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(startActivity.theme == 3) setTheme(R.style.Theme_App_black);
        setContentView(R.layout.activity_server);
        Toolbar toolbar = findViewById(R.id.toolbar);
        listView = findViewById(R.id.listView);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);

        mydb = new de.jonasled.proxdroid.DBHelper(this); //initialize the database

        fab.setOnClickListener((View v) ->  { //on click listener for add Button
            sessionInfo.databaseID = -1;
            Intent intent = new Intent(getApplicationContext(), NewServer.class);//start the activity NewServer
            startActivity(intent);
        });

        updateListView(); //buildup the Server list
        registerForContextMenu(listView);//needed for contextMenue to work

        listView.setOnItemClickListener((AdapterView<?> parent, View v, int position, long id) -> {//waits for click on a Server in the Serverlist
                try {
                    String selectedItem = databaseID.get(position);//get the ID in the database from the Server
                    int databaseID = Integer.parseInt(selectedItem);//parse the ID to int

                    Intent intent = new Intent(getApplicationContext(), ClusterActivity.class);//start the Proxmox connected class with the ID of the entry as parameter
                    sessionInfo.databaseID = databaseID;
                    startActivity(intent);
                } catch (Exception e){}
        });
        
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
    
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)//this handles the context menu
    {
        switch (v.getId()){

            case R.id.listView://check if the contextmenu is asked on the ServerList

                String [] actions = getResources().getStringArray(R.array.menue_server_list);//get the contextMenu array and add the entries to the menue
                for (int i=0;i<actions.length;i++){
                    menu.add(Menu.NONE, i, i, actions[i]);
                }
                break;

        }
    }
    @Override
    public boolean onContextItemSelected(MenuItem item){//This handles the selection on the context menu
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();//get the Item
        if(item.getItemId() == 0){//check if first item selected
            Intent intent = new Intent(getApplicationContext(), NewServer.class);//start the NewServer activity with the databaseID as parameter (used to edit the entry)
            sessionInfo.databaseID = Integer.parseInt(databaseID.get(info.position));
            startActivity(intent);
        }else if (item.getItemId() == 1){//if user selected delete, call the deleteServer function with the ID of the DB entry
            mydb.deleteServer(Integer.parseInt(databaseID.get(info.position)));
            updateListView();//regenerate the Serverlist
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu_) {//optionsmenu is the menu in the right upper corner
        String [] actions = getResources().getStringArray(R.array.menu_settings); //like on context menu get the array and make a menu of it
        for (int i=0;i<actions.length;i++){
            menu_.add(Menu.NONE, i, i, actions[i]);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {//handle the select on options menue
        // Handle item selection
        switch (item.getItemId()) {
            case 0://first entry is SettingsActivity
                Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);//open the SettingsActivity activity
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);//do nothing
        }
    }

    void updateListView(){
        ArrayList allServer = mydb.getAllServer();//get all Server IDs from Database
        ServerListAdapter adapter;
        if (allServer != null && allServer.size() > 0) { //check if the user has configured a server
            //We have entries in the Database

            ArrayList<String> names = new ArrayList<>();
            databaseID = new ArrayList<>();
            for (Object entry: allServer) { //parse all entries in the Databadr
                Cursor cursor = mydb.getData(((int) entry));//get the entry of the current server
                cursor.moveToFirst();
                names.add(cursor.getString(1));//add the Name of the server to the menu
                databaseID.add(entry.toString());//add the id of the Server
            }

            for (int i = 0; i < names.size(); i++) { //sort the nodes alphabetical
                for (int j = i + 1; j < names.size(); j++) {
                    if (names.get(i).toLowerCase().compareTo(names.get(j).toLowerCase()) > 0) {
                        String temp_ = names.get(i);
                        names.set(i, names.get(j));
                        names.set(j, temp_);
                        temp_ = databaseID.get(i);
                        databaseID.set(i, databaseID.get(j));
                        databaseID.set(j, temp_);
                    }
                }
            }

            adapter = new ServerListAdapter(this, names.toArray(new String[0]) , R.drawable.proxmox); //save the array to the adapter (use Proxmox logo as icon=
        } else {
            //database is empty
            adapter = new ServerListAdapter(this, new String[]{getResources().getString(R.string.errnoServer)} , R.drawable.icons8_cancel_480);
        }

        listView.setAdapter(adapter); //set the Adapter on the listview (show the elements)
    }
}

package de.jonasled.proxdroid;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.google.common.primitives.Ints;
import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.net.ssl.HttpsURLConnection;

import static de.jonasled.proxdroid.HelperFunctions.humanReadableByteCountBin;

public class StorageControlActivity extends AppCompatActivity {

    TextView nameText;
    TextView nodeText;
    TextView idText;
    TextView driveText;
    EditText editTextSearch;
    ListView storageList;
    Spinner graphSpinner;
    LineChart usageGraph;
    List<Entry> usageGraphSeries;
    Map<String, ArrayList<String>> storageContent = new HashMap<>();
    de.jonasled.proxdroid.ClusterListAdapter adapter;
    ArrayAdapter<CharSequence> avgMaxSpinnerValues;
    ArrayAdapter<CharSequence> graphSpinnerValues;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(startActivity.theme == 3) setTheme(R.style.Theme_App_black);
        setContentView(R.layout.activity_storage_control);
        Toolbar toolbar = findViewById(R.id.toolbar);
        nameText = findViewById(R.id.labelName);
        nodeText = findViewById(R.id.labelNode);
        idText = findViewById(R.id.labelID);
        driveText = findViewById(R.id.labelDrive);
        graphSpinner = findViewById(R.id.spinnerTime);
        usageGraph = findViewById(R.id.graphUsage);
        editTextSearch = findViewById(R.id.editTextSearch);

        storageList = findViewById(R.id.storageList);

        setSupportActionBar(toolbar);
        setTitle(sessionInfo.machineID);

        graphSpinnerValues = ArrayAdapter.createFromResource(this, R.array.spinnerGraph, android.R.layout.simple_spinner_dropdown_item);
        graphSpinner.setAdapter(graphSpinnerValues);
        graphSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    new getConfig().execute();
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
                return;
            }
        });
        avgMaxSpinnerValues = ArrayAdapter.createFromResource(this, R.array.avgMaxSpinner, android.R.layout.simple_spinner_dropdown_item);

        editTextSearch.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {

            }

            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {

            }

            public void afterTextChanged(Editable arg0) {
                ArrayList sortedKeys = new ArrayList(storageContent.keySet());//The response from proxmox is a mess, no System or whatever, so we sort the array after the key
                Collections.sort(sortedKeys);

                ArrayList<String> label = new ArrayList<>();
                ArrayList<Integer> icon = new ArrayList<>();
                ArrayList<Integer> usage = new ArrayList<>();

                for (Object key : sortedKeys.toArray()) {//parse the sorted key list
                    ArrayList<String> currentObject = storageContent.get(key);
                    if(currentObject.get(0).toLowerCase().contains(arg0.toString().toLowerCase())) {
                        usage.add(-1);
                        label.add(currentObject.get(0));
                        if (currentObject.get(1).equals("backup")) icon.add(R.drawable.backup);
                        if (currentObject.get(1).equals("iso")) icon.add(R.drawable.iso);
                        if (currentObject.get(1).equals("vztmpl")) icon.add(R.drawable.lxc_offline);
                        if (currentObject.get(1).equals("rootdir")) icon.add(R.drawable.drive);
                        if (currentObject.get(1).equals("images")) icon.add(R.drawable.lxc_running);
                    }

                    int[] iconsInt = Ints.toArray(icon);
                    int[] usageInt = Ints.toArray(usage);
                    String[] labelArray = label.toArray(new String[0]);//convert the List to array and show the listView
                    adapter = new de.jonasled.proxdroid.ClusterListAdapter(StorageControlActivity.this, labelArray, iconsInt, usageInt);
                    storageList.setAdapter(adapter);
                }
            }
        });
    }

    private class getConfig extends AsyncTask<String, String, String> {
        private ProgressDialog pd;
        JSONObject vmConfig;
        int spinnerID;
        int spinner2ID;

        @Override
        protected void onPreExecute() {
            HelperFunctions.buildKeystore();
            pd = ProgressDialog.show(StorageControlActivity.this, "", getResources().getString(R.string.loading), true,//open a loading dialog
                    false);
            spinnerID = graphSpinner.getSelectedItemPosition();
        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                URL url = new URL( sessionInfo.baseURL + "api2/json/cluster/resources");
                HttpsURLConnection conn2 = (HttpsURLConnection) url.openConnection();
                conn2.setSSLSocketFactory(HelperFunctions.context.getSocketFactory());
                conn2.setHostnameVerifier(HelperFunctions.hostnameVerifier);
                switch (sessionInfo.loginMethod){
                    case 0:
                        conn2.setRequestProperty("Cookie", "PVEAuthCookie=" + sessionInfo.ticket);//set the auth cookie
                        break;
                    case 1:
                        conn2.addRequestProperty("Authorization", sessionInfo.authString);
                        break;
                }
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn2.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    // Append server response in string
                    sb.append(line + "\n");
                }
                JSONArray resourcesJSON = (JSONArray) (new JSONObject(sb.toString())).get("data");
                for(int i=0; i < resourcesJSON.length(); i++){
                    JSONObject temp = resourcesJSON.getJSONObject(i);
                    try {
                        if (temp.getString("id").equals(sessionInfo.machineID)) {
                            vmConfig = temp;
                            break;
                        }
                    } catch (Exception e) {}
                }

                url = new URL( sessionInfo.baseURL + "api2/json/nodes/" + sessionInfo.nodeName +"/storage/" + vmConfig.getString("storage") + "/content");
                conn2 = (HttpsURLConnection) url.openConnection();
                conn2.setSSLSocketFactory(HelperFunctions.context.getSocketFactory());
                conn2.setHostnameVerifier(HelperFunctions.hostnameVerifier);
                switch (sessionInfo.loginMethod){
                    case 0:
                        conn2.setRequestProperty("Cookie", "PVEAuthCookie=" + sessionInfo.ticket);//set the auth cookie
                        break;
                    case 1:
                        conn2.addRequestProperty("Authorization", sessionInfo.authString);
                        break;
                }
                reader = new BufferedReader(new InputStreamReader(conn2.getInputStream()));
                sb = new StringBuilder();
                while ((line = reader.readLine()) != null) {
                    // Append server response in string
                    sb.append(line + "\n");
                }
                resourcesJSON = (JSONArray) (new JSONObject(sb.toString())).get("data");
                for(int i=0; i < resourcesJSON.length(); i++){
                    JSONObject temp = resourcesJSON.getJSONObject(i);
                    try {
                        String name = "none";
                        if(temp.getString("content").equals("backup")) name = temp.getString("volid").split("backup/")[1];
                        if(temp.getString("content").equals("iso")) name = temp.getString("volid").split("iso/")[1];
                        if(temp.getString("content").equals("vztmpl")) name = temp.getString("volid").split( "vztmpl/")[1];
                        if(temp.getString("content").equals("rootdir")) name = temp.getString("volid").split( temp.getInt("vmid") + "/")[1];
                        if(temp.getString("content").equals("images")) name = temp.getString("volid").split( temp.getInt("vmid") + "/")[1];

                        ArrayList<String> temp_ = new ArrayList<>();//make a array for the values
                        temp_.add(name);
                        temp_.add(temp.getString("content"));
                        temp_.add(temp.getString("size"));
                        storageContent.put(name, temp_);

                    } catch (Exception e) {}
                }

                String timeframe = "hour";
                switch (spinnerID){
                    case 0:
                        timeframe = "hour";
                        break;
                    case 1:
                        timeframe = "day";
                        break;
                    case 2:
                        timeframe = "week";
                        break;
                    case 3:
                        timeframe = "month";
                        break;
                    case 4:
                        timeframe = "year";
                        break;
                }

                url = new URL(sessionInfo.baseURL + "api2/json/nodes/" + sessionInfo.nodeName + "/storage/" + vmConfig.getString("storage") + "/rrddata?timeframe=" + timeframe);
                conn2 = (HttpsURLConnection) url.openConnection();
                conn2.setSSLSocketFactory(HelperFunctions.context.getSocketFactory());
                conn2.setHostnameVerifier(HelperFunctions.hostnameVerifier);
                switch (sessionInfo.loginMethod){
                    case 0:
                        conn2.setRequestProperty("Cookie", "PVEAuthCookie=" + sessionInfo.ticket);//set the auth cookie
                        break;
                    case 1:
                        conn2.addRequestProperty("Authorization", sessionInfo.authString);
                        break;
                }
                reader = new BufferedReader(new InputStreamReader(conn2.getInputStream()));
                sb = new StringBuilder();
                while ((line = reader.readLine()) != null) {
                    // Append server response in string
                    sb.append(line + "\n");
                }
                JSONArray graphJson = (JSONArray) (new JSONObject(sb.toString())).get("data");
                usageGraphSeries = new ArrayList<Entry>();
                for(int i=0; i < graphJson.length(); i++) {
                    JSONObject temp = graphJson.getJSONObject(i);
                    try {
                        usageGraphSeries.add(new Entry(temp.getLong("time") * 1000, (float) temp.getDouble("used")));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


            } catch (IOException e){
                e.printStackTrace();
                return "failedIO";
            } catch (JSONException e){
                e.printStackTrace();
                return "failedJSON";
            }

            return "";

        }

        @Override
        protected void onPostExecute(String result) {
            StorageControlActivity.this.runOnUiThread(() -> {
                if (result == "failedIO") {
                    AlertDialog.Builder dlgAlert = new AlertDialog.Builder(StorageControlActivity.this);
                    dlgAlert.setMessage(R.string.errNotReachable);
                    dlgAlert.setTitle(R.string.errNotReachableTitle);
                    dlgAlert.setCancelable(false);
                    dlgAlert.setPositiveButton(R.string.ok,(DialogInterface dialog, int which) -> finish());
                    dlgAlert.create().show();
                } else if (result == "failedJSON") {
                    AlertDialog.Builder dlgAlert = new AlertDialog.Builder(StorageControlActivity.this);
                    dlgAlert.setMessage(R.string.errInvalidJson);
                    dlgAlert.setTitle(R.string.errInvalidJsonTitle);
                    dlgAlert.setCancelable(false);
                    dlgAlert.setPositiveButton(R.string.ok,(DialogInterface dialog, int which) -> finish());
                    dlgAlert.create().show();
                }
                try{
                    setTitle(sessionInfo.nodeName);
                    nameText.setText(sessionInfo.nodeName);
                    idText.setText(sessionInfo.machineID);
                    driveText.setText(humanReadableByteCountBin(vmConfig.getLong("disk")) + " / " + humanReadableByteCountBin(vmConfig.getLong("maxdisk")));

                } catch (JSONException e){
                    e.printStackTrace();
                    AlertDialog.Builder dlgAlert = new AlertDialog.Builder(StorageControlActivity.this);
                    dlgAlert.setMessage(R.string.errInvalidJson);
                    dlgAlert.setTitle(R.string.errInvalidJsonTitle);
                    dlgAlert.setCancelable(false);
                    dlgAlert.setPositiveButton(R.string.ok,(DialogInterface dialog, int which) -> finish());
                    dlgAlert.create().show();
                }

                ArrayList sortedKeys = new ArrayList(storageContent.keySet());//The response from proxmox is a mess, no System or whatever, so we sort the array after the key
                Collections.sort(sortedKeys);

                ArrayList<String> label = new ArrayList<>();
                ArrayList<Integer> icon = new ArrayList<>();
                ArrayList<Integer> usage = new ArrayList<>();

                for (Object key : sortedKeys.toArray()) {//parse the sorted key list
                    usage.add(-1);
                    ArrayList<String> currentObject = storageContent.get(key);
                    label.add(currentObject.get(0));
                    if(currentObject.get(1).equals("backup")) icon.add(R.drawable.backup);
                    if(currentObject.get(1).equals("iso")) icon.add(R.drawable.iso);
                    if(currentObject.get(1).equals("vztmpl")) icon.add(R.drawable.lxc_offline);
                    if(currentObject.get(1).equals("rootdir")) icon.add(R.drawable.drive);
                    if(currentObject.get(1).equals("images")) icon.add(R.drawable.lxc_running);

                    int[] iconsInt = Ints.toArray(icon);
                    int[] usageInt = Ints.toArray(usage);
                    String[] labelArray = label.toArray(new String[0]);//convert the List to array and show the listView
                    adapter = new de.jonasled.proxdroid.ClusterListAdapter(StorageControlActivity.this, labelArray, iconsInt, usageInt);
                    storageList.setAdapter(adapter);

                    graphHandler.setupGraph(usageGraph, graphSpinner.getSelectedItemPosition() <= 1, true, true, StorageControlActivity.this, usageGraphSeries);
                }
            });

            pd.dismiss();
        }

    }
}

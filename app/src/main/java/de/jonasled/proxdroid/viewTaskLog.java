package de.jonasled.proxdroid;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import com.google.common.primitives.Ints;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

public class viewTaskLog extends AppCompatActivity {
    TextView logText;
    String taskName;
    int loginMethod;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(startActivity.theme == 3) setTheme(R.style.Theme_App_black);
        setContentView(R.layout.activity_view_task_log);
        Toolbar toolbar = findViewById(R.id.toolbar3);
        setSupportActionBar(toolbar);
        setTitle(getResources().getString(R.string.app_name) + " - " + getResources().getString(R.string.tasks));
        logText = findViewById(R.id.textView4);
        taskName = getIntent().getStringExtra("taskName");

        new getConfig().execute();

    }

    private class getConfig extends AsyncTask<String, String, String> {
        private ProgressDialog pd;
        String taskLog = "";

        @Override
        protected void onPreExecute() {
            HelperFunctions.buildKeystore();
            pd = ProgressDialog.show(viewTaskLog.this, "", getResources().getString(R.string.loading), true,//open a loading dialog
                    false);
        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                String nodeName = taskName.split(":")[1];
                URL url = new URL( sessionInfo.baseURL + "api2/extjs/nodes/" + nodeName + "/tasks/" + taskName + "/log");
                HttpsURLConnection conn2 = (HttpsURLConnection) url.openConnection();
                conn2.setSSLSocketFactory(HelperFunctions.context.getSocketFactory());
                conn2.setHostnameVerifier(HelperFunctions.hostnameVerifier);
                switch (loginMethod){
                    case 0:
                        conn2.setRequestProperty("Cookie", "PVEAuthCookie=" + sessionInfo.ticket);//set the auth cookie
                        conn2.setRequestProperty("CSRFPreventionToken", sessionInfo.CSRFPreventionToken);
                        break;
                    case 1:
                        conn2.addRequestProperty("Authorization", sessionInfo.authString);
                        break;
                }
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn2.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    // Append server response in string
                    sb.append(line + "\n");
                }
                JSONArray resourcesJSON = (new JSONObject(sb.toString())).getJSONArray("data");
                for(int i=0; i < resourcesJSON.length(); i++){
                    JSONObject temp = resourcesJSON.getJSONObject(i);
                    taskLog += temp.getString("t") + "\n";
                }


            } catch (IOException e){
                e.printStackTrace();
                return "failedIO";
            } catch (JSONException e){
                e.printStackTrace();
                return "failedJSON";
            }

            return "";

        }

        @Override
        protected void onPostExecute(String result) {
            logText.setText(taskLog);
            logText.setMovementMethod(new ScrollingMovementMethod());

            pd.dismiss();
        }

    }
}
package de.jonasled.proxdroid;

import java.util.ArrayList;
import java.util.HashMap;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;

public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "database.db";
    Cursor res;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME , null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table servers (id integer primary key, name text,url text,realm text, username text,password text, requiresTOTP boolean, totpToken text, loginType INTEGER DEFAULT 0 )"
        );
    }

    public void appUpdate(){
        SQLiteDatabase db = this.getWritableDatabase();
        try{
            db.execSQL("ALTER TABLE servers ADD COLUMN loginType INTEGER DEFAULT 0");
        } catch (Exception e) {}

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS servers");
        onCreate(db);
    }

    public boolean insertServer (String name, String url, String realm, String username,String password, Boolean requiresTOTP, String totpToken, int loginType) {  //insert a new Server into the Database
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);
        contentValues.put("url", url);
        contentValues.put("realm", realm);
        contentValues.put("username", username);
        contentValues.put("password", password);
        contentValues.put("requiresTOTP", requiresTOTP);
        contentValues.put("totpToken", totpToken);
        contentValues.put("loginType", loginType);
        db.insert("servers", null, contentValues);
        return true;
    }

    public Cursor getData(int id) { //get the database entry for a specific ID
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from servers where id=" + id +"", null );
        return res;
    }


    public Integer deleteServer (Integer id) { //delete the database entry for a specific ID
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("servers",
                "id = ? ",
                new String[] { Integer.toString(id) });
    }

    public ArrayList<Integer> getAllServer() { //get the IDs for all Server
        ArrayList<Integer> array_list = new ArrayList<Integer>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        res = db.rawQuery( "select * from servers", null );
        res.moveToFirst();

        while(!res.isAfterLast()){
            array_list.add(res.getInt(res.getColumnIndex("id")));
            res.moveToNext();
        }
        return array_list;
    }
}
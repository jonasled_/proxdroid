package de.jonasled.proxdroid;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.common.primitives.Ints;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;

import javax.net.ssl.HttpsURLConnection;

public class backupCreateActivity extends AppCompatActivity {
    FloatingActionButton fab;
    Spinner storageSpinner;
    Spinner modeSpinner;
    Spinner compressionSpinner;
    EditText emailInput;
    ArrayList<String> backupStorages = new ArrayList<>();
    ArrayAdapter<CharSequence> backupModeAdapter;
    ArrayAdapter<CharSequence> backupCompressionAdapter;
    ArrayAdapter<String> backupStoragesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(startActivity.theme == 3) setTheme(R.style.Theme_App_black);
        setContentView(R.layout.activity_backup_create);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fab = findViewById(R.id.fab);
        storageSpinner = findViewById(R.id.spinnerStorage);
        modeSpinner = findViewById(R.id.spinnerMode);
        compressionSpinner = findViewById(R.id.spinnerCompression);
        emailInput = findViewById(R.id.inputEmail);

        setTitle(sessionInfo.machineName + " - " + getResources().getString(R.string.backup));
        fab.setOnClickListener(view -> {
            new createBackupTask().execute();
        });

        new getConfig().execute();
    }

    private class createBackupTask extends AsyncTask<String, String, String> {
        private ProgressDialog pd;
        private String storage;
        private String mode;
        private String mailto;
        private String compress;

        @Override
        protected void onPreExecute() {
            HelperFunctions.buildKeystore();
            pd = ProgressDialog.show(backupCreateActivity.this, "", getResources().getString(R.string.loading), true,//open a loading dialog
                    false);
            storage = backupStorages.get(storageSpinner.getSelectedItemPosition());
            switch (modeSpinner.getSelectedItemPosition()){
                case 0:
                    mode = "snapshot";
                    break;
                case 1:
                    mode = "suspend";
                    break;
                case 2:
                    mode = "stop";
                    break;
            }

            switch(compressionSpinner.getSelectedItemPosition()){
                case 0:
                    compress = "0";
                    break;
                case 1:
                    compress = "gzip";
                    break;
                case 2:
                    compress = "lzo";
                    break;
                case 3:
                    compress = "zstd";
                    break;
            }

            mailto = emailInput.getText().toString();

        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                URL url = new URL( sessionInfo.baseURL + "api2/extjs/nodes/" + sessionInfo.nodeName + "/vzdump");
                HttpsURLConnection conn2 = (HttpsURLConnection) url.openConnection();
                conn2.setSSLSocketFactory(HelperFunctions.context.getSocketFactory());
                conn2.setHostnameVerifier(HelperFunctions.hostnameVerifier);
                conn2.setRequestProperty("Cookie", "PVEAuthCookie=" + sessionInfo.ticket);
                conn2.setRequestProperty("CSRFPreventionToken", sessionInfo.CSRFPreventionToken);
                conn2.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn2.getOutputStream());//write the POST data
                wr.write("storage=" + storage + "&vmid=" + sessionInfo.machineID + "&mode=" + mode + "&remove=0&mailto=" + mailto + "&compress=" + compress);
                wr.flush();
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn2.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    // Append server response in string
                    sb.append(line + "\n");
                }
                if(new JSONObject(sb.toString()).getInt("success") == 1){
                    return "OK";
                }


            } catch (IOException | JSONException e){
                e.printStackTrace();
                return "failedIO";
            }

            return "";

        }

        @Override
        protected void onPostExecute(String result) {

            pd.dismiss();
            if(result == "OK"){
                Toast.makeText(getApplicationContext(),R.string.backupStarted,Toast.LENGTH_SHORT).show();
                finish();
            }
        }

    }

    private class getConfig extends AsyncTask<String, String, String> {
        private ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            HelperFunctions.buildKeystore();
            pd = ProgressDialog.show(backupCreateActivity.this, "", getResources().getString(R.string.loading), true,//open a loading dialog
                    false);
            backupStorages = new ArrayList<>();
        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                URL url = new URL( sessionInfo.baseURL + "api2/json/nodes/" + sessionInfo.nodeName + "/storage?format=1&content=backup");
                HttpsURLConnection conn2 = (HttpsURLConnection) url.openConnection();
                conn2.setSSLSocketFactory(HelperFunctions.context.getSocketFactory());
                conn2.setHostnameVerifier(HelperFunctions.hostnameVerifier);
                conn2.setRequestProperty("Cookie", "PVEAuthCookie=" + sessionInfo.ticket);
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn2.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    // Append server response in string
                    sb.append(line + "\n");
                }
                JSONArray resourcesJSON = (JSONArray) (new JSONObject(sb.toString())).get("data");
                for(int i=0; i < resourcesJSON.length(); i++){
                    JSONObject temp = resourcesJSON.getJSONObject(i);
                    try {
                        backupStorages.add(temp.getString("storage"));
                    } catch (Exception e) {e.printStackTrace();}
                }


            } catch (IOException e){
                e.printStackTrace();
                return "failedIO";
            } catch (JSONException e){
                e.printStackTrace();
                return "failedJSON";
            }

            return "";

        }

        @Override
        protected void onPostExecute(String result) {
            backupStoragesAdapter = new ArrayAdapter<String>(backupCreateActivity.this, R.layout.support_simple_spinner_dropdown_item, backupStorages);
            backupModeAdapter = ArrayAdapter.createFromResource(backupCreateActivity.this, R.array.backupMode, R.layout.support_simple_spinner_dropdown_item);
            backupCompressionAdapter = ArrayAdapter.createFromResource(backupCreateActivity.this, R.array.backupCompressionList, R.layout.support_simple_spinner_dropdown_item);

            storageSpinner.setAdapter(backupStoragesAdapter);
            modeSpinner.setAdapter(backupModeAdapter);
            compressionSpinner.setAdapter(backupCompressionAdapter);

            pd.dismiss();
        }

    }
}
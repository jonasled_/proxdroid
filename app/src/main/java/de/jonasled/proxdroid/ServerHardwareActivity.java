package de.jonasled.proxdroid;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ListView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import com.google.common.primitives.Ints;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

public class ServerHardwareActivity extends AppCompatActivity {

    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_server_hardware);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        listView = findViewById(R.id.listView);

        setTitle(sessionInfo.machineName + " - " + getResources().getString(R.string.hardware));

        new getHardware().execute();
    }


    private class getHardware extends AsyncTask<String, String, String> {
        private ProgressDialog pd;
        private String description = "";
        JSONArray resourcesJSON;
        ClusterListAdapter adapter;
        ArrayList<String> label = new ArrayList<>();
        ArrayList<Integer> icon = new ArrayList<>();
        ArrayList<Integer> storageUsage = new ArrayList<>();


        @Override
        protected void onPreExecute() {
            HelperFunctions.buildKeystore();
            super.onPreExecute();
            pd = ProgressDialog.show(ServerHardwareActivity.this, "", getResources().getString(R.string.loading), true,
                    false); // Create and show Progress dialog

        }
        @Override
        protected String doInBackground(String... strings) {
            try {
                URL url = new URL(sessionInfo.baseURL + "api2/json/nodes/" + sessionInfo.nodeName + "/" + sessionInfo.machineType + "/" + sessionInfo.machineID + "/pending");
                HttpsURLConnection conn2 = (HttpsURLConnection) url.openConnection();
                conn2.setSSLSocketFactory(HelperFunctions.context.getSocketFactory());
                conn2.setHostnameVerifier(HelperFunctions.hostnameVerifier);
                switch (sessionInfo.loginMethod){
                    case 0:
                        conn2.setRequestProperty("Cookie", "PVEAuthCookie=" + sessionInfo.ticket);//set the auth cookie
                        break;
                    case 1:
                        conn2.addRequestProperty("Authorization", sessionInfo.authString);
                        break;
                }
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn2.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    // Append server response in string
                    sb.append(line + "\n");
                }
                resourcesJSON = (JSONArray) (new JSONObject(sb.toString())).get("data");
                String cores = "";
                String sockets = "";

                for (int i=0; i < resourcesJSON.length(); i++) {
                    JSONObject hardwareItem = resourcesJSON.getJSONObject(i);
                    String key = hardwareItem.getString("key");

                    storageUsage.add(-1);
                    if(key.equals("memory")){
                        icon.add(R.drawable.ram);
                        label.add(HelperFunctions.humanReadableByteCountBin(hardwareItem.getLong("value") * 1048576));
                    } else if(key.equals("swap")){
                        icon.add(R.drawable.swap);
                        label.add(HelperFunctions.humanReadableByteCountBin(hardwareItem.getLong("value") * 1048576));
                    } else if(key.startsWith("ide") || key.startsWith("scsi") || key.startsWith("sata") || key.startsWith("virtio")){
                        String value = hardwareItem.getString("value");
                        if(value.contains("media=cdrom")){
                            icon.add(R.drawable.cd);
                        } else {
                            icon.add(R.drawable.drive);
                        }
                        label.add(value);

                    } else if(key.startsWith("net")){
                        icon.add(R.drawable.network);
                        label.add(hardwareItem.getString("value"));

                    } else if(key.startsWith("rng")){
                        icon.add(R.drawable.rng);
                        label.add(hardwareItem.getString("value"));

                    } else if(key.startsWith("scsihw") || key.startsWith("mp") || key.equals("rootfs")){
                        icon.add(R.drawable.storage_online);
                        label.add(hardwareItem.getString("value"));

                    } else if(key.startsWith("usb")){
                        icon.add(R.drawable.usb);
                        label.add(hardwareItem.getString("value"));

                    } else if(key.equals("cores")) {
                        cores = hardwareItem.getString("value");

                    } else if(key.startsWith("serial")) {
                        icon.add(R.drawable.serial);
                        label.add(hardwareItem.getString("value"));

                    } else if(key.startsWith("audio")) {
                        icon.add(R.drawable.audio);
                        label.add(hardwareItem.getString("value"));

                    } else if(key.equals("sockets")) {
                        sockets = hardwareItem.getString("value");
                        storageUsage.remove(0);

                    }else if(key.equals("hostpcie")) {
                        icon.add(R.drawable.pcie);
                        label.add(hardwareItem.getString("value"));

                    }
                }
                if(sessionInfo.machineType.equals("lxc")){
                    icon.add(R.drawable.processor);
                    label.add("Cores: " + cores);
                } else {
                    icon.add(R.drawable.processor);
                    label.add(getResources().getString(R.string.sockets) + ": " + sockets + ", " + getResources().getString(R.string.cores) + ": " + cores);
                }

            } catch (IOException e){
                e.printStackTrace();
                return "failedIO";
            } catch (JSONException e){
                e.printStackTrace();
                return "failedJSON";
            }
            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            pd.dismiss();
            if (result == "failedIO") {
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(ServerHardwareActivity.this);
                dlgAlert.setMessage(R.string.errNotReachable);
                dlgAlert.setTitle(R.string.errNotReachableTitle);
                dlgAlert.setCancelable(false);
                dlgAlert.setPositiveButton(R.string.ok,(DialogInterface dialog, int which) -> finish());
                dlgAlert.create().show();
            } else if (result == "failedJSON") {
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(ServerHardwareActivity.this);
                dlgAlert.setMessage(R.string.errInvalidJson);
                dlgAlert.setTitle(R.string.errInvalidJsonTitle);
                dlgAlert.setCancelable(false);
                dlgAlert.setPositiveButton(R.string.ok,(DialogInterface dialog, int which) -> finish());
                dlgAlert.create().show();
            }


            int[] iconsInt = Ints.toArray(icon);
            int[] usageInt = Ints.toArray(storageUsage);
            String[] labelArray = label.toArray(new String[0]);//convert the List to array and show the listView
            adapter = new de.jonasled.proxdroid.ClusterListAdapter(ServerHardwareActivity.this, labelArray, iconsInt, usageInt);
            listView.setAdapter(adapter);
        }
    }

}
# Proxdroid

The free and easy to use Proxmox VE App for Android to watch and control your machines

![pipeline status](https://gitlab.com/jonasled_/proxdroid/badges/master/pipeline.svg) ![](http://translate.jonasled.de/widgets/proxdroid/-/translation/svg-badge.svg)

## Features
* See the status of your Machines
* Control your machines (Start / Stop and via VNC)
* Authenticate with users, which require TOTP (google authenticator)

![](https://lh3.googleusercontent.com/Vs1ydUioE_kYWvOPyMdq_VZZpdS2-0hQF_J8CtOGYMidykCOIDnR9AiAsRUK1FUeGrdL=w1920-h920-rw)

## Translation
I have a website, where you can translate the App in different languages, to support as many languages as possible. The current status can be found below. 
The main page can be found [here](https://translate.jonasled.de/engage/proxdroid/?utm_source=widget). If a language is missing, please open a issue here on gitlab and I'll add it as soon as possible.

![translation status](https://translate.jonasled.de/widgets/proxdroid/-/translation/multi-auto.svg)

##  Support
* Discord: [https://discord.gg/JEuXW8h](https://discord.gg/JEuXW8h)
* Open a issue [here](https://gitlab.com/jonasled_/proxdroid/-/issues/new)
